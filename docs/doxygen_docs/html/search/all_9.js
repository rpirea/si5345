var searchData=
[
  ['mdsplloutfrequency_23',['mDSPLLOutFrequency',['../classsi5345.html#a157acc0641023a4cd2356b50148c90c1',1,'si5345']]],
  ['minputchannel_5ffreq_24',['mInputChannel_freq',['../classsi5345.html#a776aacc31a5f4f49484488bba4bfd100',1,'si5345']]],
  ['minterface_25',['mInterface',['../classsi5345.html#a2bf93b724ab466c839f685feb8f5a253',1,'si5345']]],
  ['multisynth_5fout_5fmin_5ffreq_26',['MULTISYNTH_OUT_MIN_FREQ',['../si5345__defines_8hpp.html#a8c606317d2762974ca2f2b8bd5952d4e',1,'si5345_defines.hpp']]],
  ['mxtalfreq_27',['mXtalFreq',['../classsi5345.html#a2eb473d51a87fd600c0f627b2aee9829',1,'si5345']]]
];
