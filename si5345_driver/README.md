# CERN-driver_si5345

CERN-driver_si5345




                            Usage instructions:
                            
                            
 
1) instantiate object of type si5345
ex:
    si5345 si5345_instance(si5345::comm_type::SPI, file, filepath, mXtalFreq);
        comm_type can be SPI or I2C
        file used for I2C
        filepath used for SPI
        mXtalFreq default 48MHz
 
FREERUN MODE: 
 
    2) enable freerun mode: 
    ex:
        si5345_instance.enable_freerun_mode();
        
    3) set the desired frequency on the specific output channel (also enables the output channel)
    ex:
        si5345_instance.set_frequency(freq, output_channel);
                   
LOCK AFTER INPUT MODE: 

    2) enable input lock mode: 
    ex:
        si5345_instance.set_registers_input_controlled();

    3) set the desired frequency on the specific input channel after which the lock happens
    ex:
        si5345_instance.select_input(input_channel,frequency);
    
    4) set the desired frequency on the specific output channel (also enables the output channel)
    ex:
        si5345_instance.set_frequency(freq, output_channel);
                   

                   

                            Known bugs:
                            
1) Some frequency combinations are invalid as their lowest common divider
    exceeds 13.3 GHz dspll out frequency and requires improper n divider values.
    n divider <-> output channel rerouting algorithm is not implemented to 
    combat this situation.

2) input frequencies under 70k Hz do not lock properly 





                                 TODO:
   
1) reordering/rerouting optimization for multysinth<->output channel connections


        
        
         
                            IMPORTANT:
                            
                            
stuff to take care of / take into account: 

1) c++ 14 needed for binary writing (0b si 0x )

2) https://ro.farnell.com/silicon-labs/si570-prog-evb/eval-board-i2c-programmable-oscillator/dp/2930698    -   SI570-PROG-EVB
https://ro.farnell.com/silicon-labs/si5345-d-evb/eval-board-pll-jitter-attenuator/dp/2930696            -   SI5345-D-EVB

3) real time processing not needed

4)if changing code:  some regs do not have normal base + offset addresses
search for "special case" in order to find them

5) tested for:
input freq: 70k -> 10M
output freq: 100k -> 10M

6) requires xaxb clock
    quartz of 48 MHz



















