

#ifndef si5345_tests___cpp
#define si5345_tests___cpp

#include <cstdlib>
#include <cstdint>
#include <map>
#include <utility>
#include <si5345/si5345.hpp>
#include <tests/si5345_tests.h>
#include <cassert>
#include <fcntl.h>
#include <cerrno>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <cstring>
#include <unistd.h>
#include <cstdio>
#include <unistd.h>
#include <cstdint>
#include <cstdint>
#include <cstdint>
#include <elf.h>
#include "si5345/si5345.hpp"
#include "si5345/si5345_interface.hpp"
#include "si5345/si5345_interface_i2c.hpp"
#include "si5345/si5345_interface_spi.hpp"



using namespace std;





/*! function used to run the complete test suite based on the selected settings 
settings are controlled through the macros found at the top of the file
arguments: si5345 instance on which to run
returns: none
*/
void run_tests(si5345 & si5345_instance)
{
	si5345_instance.reset_hard();
	
	printf("Starting test suite");
	srand(SEED);
	printf("Seed = %d\n\n\n", SEED);
	
	run_test_read_id(si5345_instance);
	
    run_test_manual_test(si5345_instance);
	//run_test_freerun_out0(si5345_instance,1000000);
	
	//si5345_instance.test_function();
    printf("manual test passed\n\n");
    
    run_test_read_id(si5345_instance);
	
    printf("Read id test passed\n\n");
    
    printf("Finished test suite");
	
}//end function: run_tests



/*! function used to manually test features
used for debugging
arguments: si5345 instance on which to run
returns: none
*/
void run_test_manual_test(si5345 & si5345_instance)
{
	si5345_instance.reset_hard();
	
	
	//add code here
	
	
	usleep(15 * 1000 * 1000);
	
	//si5345_instance.dump_registers();
	//si5345_instance.test_function();
	printf("DONE!!!!!!!!!!!!!");
	//si5345_instance.dump_registers();
	printf("\n\n\n\n\n\n");
}//endfunction: run_test_manual_test



/*! function used to manually test features, last test run
 changing output and input frequencies and channels
used for debugging
arguments: si5345 instance on which to run
returns: none
*/
void run_test_final_test(si5345 & si5345_instance)
{
	si5345_instance.reset_hard();
	
	si5345_instance.set_registers_freerun();
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(3000000,0);
	si5345_instance.set_frequency(4000000,0);
	si5345_instance.set_frequency(4000000,1);
	si5345_instance.set_frequency(4000000,2);
	si5345_instance.set_frequency(4000000,3);
	si5345_instance.set_frequency(4000000,4);
	si5345_instance.set_frequency(4000000,5);
	si5345_instance.set_frequency(4000000,6);
	si5345_instance.set_frequency(4000000,7);
	si5345_instance.set_frequency(4000000,8);
	si5345_instance.set_frequency(4000000,9);
	
	si5345_instance.set_registers_input_controlled(); //in 0,1,2,3 - 1MHZ
	si5345_instance.select_input(1,2000000);
	si5345_instance.set_frequency(3000000,0);
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(2000000,1);
	si5345_instance.set_frequency(2000000,2);
	si5345_instance.set_frequency(2000000,3);
	si5345_instance.set_frequency(2000000,4);
	si5345_instance.set_frequency(2000000,5);
	si5345_instance.set_frequency(2000000,6);
	si5345_instance.set_frequency(2000000,7);
	si5345_instance.set_frequency(2000000,8);
	si5345_instance.set_frequency(2000000,9);
	
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(1000000,1);
	si5345_instance.set_frequency(1000000,2);
	si5345_instance.set_frequency(1000000,3);
	si5345_instance.set_frequency(1000000,4);
	si5345_instance.set_frequency(1000000,5);
	si5345_instance.set_frequency(1000000,6);
	si5345_instance.set_frequency(1000000,7);
	si5345_instance.set_frequency(1000000,8);
	si5345_instance.set_frequency(1000000,9);
	
	si5345_instance.set_registers_freerun();
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(3000000,0);
	si5345_instance.set_frequency(4000000,0);
	si5345_instance.set_frequency(4000000,1);
	si5345_instance.set_frequency(4000000,2);
	si5345_instance.set_frequency(4000000,3);
	si5345_instance.set_frequency(4000000,4);
	si5345_instance.set_frequency(4000000,5);
	si5345_instance.set_frequency(4000000,6);
	si5345_instance.set_frequency(4000000,7);
	si5345_instance.set_frequency(4000000,8);
	si5345_instance.set_frequency(4000000,9);
	
	si5345_instance.set_registers_input_controlled(); //in 0,1,2,3 - 1MHZ
	si5345_instance.select_input(1,2000000);
	si5345_instance.set_frequency(3000000,0);
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(2000000,1);
	si5345_instance.set_frequency(2000000,2);
	si5345_instance.set_frequency(2000000,3);
	si5345_instance.set_frequency(2000000,4);
	si5345_instance.set_frequency(2000000,5);
	si5345_instance.set_frequency(2000000,6);
	si5345_instance.set_frequency(2000000,7);
	si5345_instance.set_frequency(2000000,8);
	si5345_instance.set_frequency(2000000,9);
	
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(1000000,1);
	si5345_instance.set_frequency(1000000,2);
	si5345_instance.set_frequency(1000000,3);
	si5345_instance.set_frequency(1000000,4);
	si5345_instance.set_frequency(1000000,5);
	si5345_instance.set_frequency(1000000,6);
	si5345_instance.set_frequency(1000000,7);
	si5345_instance.set_frequency(1000000,8);
	si5345_instance.set_frequency(1000000,9);
	
	si5345_instance.set_registers_input_controlled(); //in 0,1,2,3 - 1MHZ
	si5345_instance.select_input(1,3000000);
	si5345_instance.set_frequency(3000000,0);
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(2000000,1);
	si5345_instance.set_frequency(2000000,2);
	si5345_instance.set_frequency(2000000,3);
	si5345_instance.set_frequency(2000000,4);
	si5345_instance.set_frequency(2000000,5);
	si5345_instance.set_frequency(2000000,6);
	si5345_instance.set_frequency(2000000,7);
	si5345_instance.set_frequency(2000000,8);
	si5345_instance.set_frequency(2000000,9);
	
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(1000000,1);
	si5345_instance.set_frequency(1000000,2);
	si5345_instance.set_frequency(1000000,3);
	si5345_instance.set_frequency(1000000,4);
	si5345_instance.set_frequency(1000000,5);
	si5345_instance.set_frequency(1000000,6);
	si5345_instance.set_frequency(1000000,7);
	si5345_instance.set_frequency(1000000,8);
	si5345_instance.set_frequency(1000000,9);
	
	si5345_instance.set_registers_input_controlled(); //in 0,1,2,3 - 1MHZ
	si5345_instance.select_input(0,2000000);
	si5345_instance.set_frequency(3000000,0);
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(2000000,1);
	si5345_instance.set_frequency(2000000,2);
	si5345_instance.set_frequency(2000000,3);
	si5345_instance.set_frequency(2000000,4);
	si5345_instance.set_frequency(2000000,5);
	si5345_instance.set_frequency(2000000,6);
	si5345_instance.set_frequency(2000000,7);
	si5345_instance.set_frequency(2000000,8);
	si5345_instance.set_frequency(2000000,9);
	
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(1000000,1);
	si5345_instance.set_frequency(1000000,2);
	si5345_instance.set_frequency(1000000,3);
	si5345_instance.set_frequency(1000000,4);
	si5345_instance.set_frequency(1000000,5);
	si5345_instance.set_frequency(1000000,6);
	si5345_instance.set_frequency(1000000,7);
	si5345_instance.set_frequency(1000000,8);
	si5345_instance.set_frequency(1000000,9);
	
}//endfunction: run_test_final_test



/*! function used to manually test features,
 changing output and input frequencies and channels
used for debugging
arguments: si5345 instance on which to run
returns: none
*/
void run_test_input_and_output_frequencies(si5345 & si5345_instance)
{
	si5345_instance.reset_hard();
	
	si5345_instance.set_registers_input_controlled(); //in 0,1,2,3 - 1MHZ
	si5345_instance.reset_soft_all();
	si5345_instance.select_input(3,1000000);
	
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(2000000,1);
	si5345_instance.set_frequency(2000000,2);
	si5345_instance.set_frequency(2000000,3);
	si5345_instance.set_frequency(2000000,4);
	si5345_instance.set_frequency(2000000,5);
	si5345_instance.set_frequency(2000000,6);
	si5345_instance.set_frequency(2000000,7);
	si5345_instance.set_frequency(2000000,8);
	si5345_instance.set_frequency(2000000,9);
	
	si5345_instance.select_input(3,1000000);
	si5345_instance.set_frequency(3000000,1);
	si5345_instance.set_frequency(3000000,2);
	si5345_instance.set_frequency(3000000,3);
	si5345_instance.set_frequency(3000000,4);
	si5345_instance.set_frequency(3000000,5);
	si5345_instance.set_frequency(3000000,6);
	si5345_instance.set_frequency(3000000,7);
	si5345_instance.set_frequency(3000000,8);
	si5345_instance.set_frequency(3000000,9);
	si5345_instance.set_frequency(3000000,0);
	
	si5345_instance.select_input(3,3000000);
	si5345_instance.set_frequency(4000000,1);
	si5345_instance.set_frequency(4000000,2);
	si5345_instance.set_frequency(4000000,3);
	si5345_instance.set_frequency(4000000,4);
	si5345_instance.set_frequency(4000000,5);
	si5345_instance.set_frequency(4000000,6);
	si5345_instance.set_frequency(4000000,7);
	si5345_instance.set_frequency(4000000,8);
	si5345_instance.set_frequency(4000000,9);
	si5345_instance.set_frequency(4000000,0);
	
	si5345_instance.select_input(3,4000000);
	si5345_instance.set_frequency(5000000,1);
	si5345_instance.set_frequency(5000000,2);
	si5345_instance.set_frequency(5000000,3);
	si5345_instance.set_frequency(5000000,4);
	si5345_instance.set_frequency(5000000,5);
	si5345_instance.set_frequency(5000000,6);
	si5345_instance.set_frequency(5000000,7);
	si5345_instance.set_frequency(5000000,8);
	si5345_instance.set_frequency(5000000,9);
	si5345_instance.set_frequency(5000000,0);
	si5345_instance.set_frequency(4000000,0);
	si5345_instance.set_frequency(3000000,0);
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(500000,0);
	si5345_instance.set_frequency(400000,0);
	si5345_instance.set_frequency(300000,0);
	si5345_instance.set_frequency(200000,0);
	
	si5345_instance.select_input(1,2000000);
	si5345_instance.set_frequency(2000000,1);
	si5345_instance.set_frequency(2000000,2);
	si5345_instance.set_frequency(2000000,3);
	si5345_instance.set_frequency(2000000,4);
	si5345_instance.set_frequency(2000000,5);
	si5345_instance.set_frequency(2000000,6);
	si5345_instance.set_frequency(2000000,7);
	si5345_instance.set_frequency(2000000,8);
	si5345_instance.set_frequency(2000000,9);
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(4000000,0);
	si5345_instance.set_frequency(3000000,0);
	si5345_instance.set_frequency(2000000,0);
	si5345_instance.set_frequency(1000000,0);
	si5345_instance.set_frequency(500000,0);
	si5345_instance.set_frequency(400000,0);
	si5345_instance.set_frequency(300000,0);
	si5345_instance.set_frequency(200000,0);
	
}//endfunction: run_test_input_and_output_frequencies



/*! function used to test lock from different freq and inputs
arguments: si5345 instance on which to run
returns: none
*/
void run_test_input_freq_test(si5345 & si5345_instance)
{
	si5345_instance.reset_hard();
	
	printf("phase 0\n" );
	
	si5345_instance.set_registers_input_controlled(); //in 0,1,2,3 - 1MHZ
	
	for(int i = 0 ; i < 4 ; i++)
	{
		printf("input %d stuff\n\n\n",i );
		
		si5345_instance.select_input(i,1000000);	// 1M ++ , by million
		//si5345_instance.dump_registers();
		//si5345_instance.test_function();
		si5345_instance.select_input(i,2000000);
		si5345_instance.select_input(i,3000000);
		si5345_instance.select_input(i,4000000);
		si5345_instance.select_input(i,5000000);
		si5345_instance.select_input(i,6000000);
		si5345_instance.select_input(i,7000000);
		si5345_instance.select_input(i,8000000);
		si5345_instance.select_input(i,9000000);
		si5345_instance.select_input(i,10000000);	// 1M ++, by 100k
		si5345_instance.select_input(i,1100000);
		si5345_instance.select_input(i,1200000);
		si5345_instance.select_input(i,1300000);
		si5345_instance.select_input(i,1400000);
		si5345_instance.select_input(i,1500000);
		si5345_instance.select_input(i,1600000);
		si5345_instance.select_input(i,1700000);
		si5345_instance.select_input(i,1800000);
		si5345_instance.select_input(i,1900000);
		si5345_instance.select_input(i,1010000);	// 1M ++, by 10k
		si5345_instance.select_input(i,1020000);
		si5345_instance.select_input(i,1030000);
		si5345_instance.select_input(i,1040000);
		si5345_instance.select_input(i,1050000);
		si5345_instance.select_input(i,1060000);
		si5345_instance.select_input(i,1070000);
		si5345_instance.select_input(i,1080000);
		si5345_instance.select_input(i,1090000);
		
		si5345_instance.select_input(i,1000000);	// random values
		si5345_instance.select_input(i,3500000);
		si5345_instance.select_input(i,4300000);
		
		si5345_instance.select_input(i,1000000);		// under 1 M
		si5345_instance.select_input(i,500000);
		si5345_instance.select_input(i,400000);
		si5345_instance.select_input(i,300000);
		si5345_instance.select_input(i,200000);
		si5345_instance.select_input(i,100000);
		
		si5345_instance.select_input(i,100000);			// under 100 k
		si5345_instance.select_input(i,90000);
		si5345_instance.select_input(i,80000);
		si5345_instance.select_input(i,70000);
		si5345_instance.select_input(i,60000);	// problems here and below
		si5345_instance.select_input(i,50000);
		si5345_instance.select_input(i,40000);
		si5345_instance.select_input(i,30000);
		si5345_instance.select_input(i,20000);
		si5345_instance.select_input(i,10000);
		
		si5345_instance.select_input(i,1000000);
		
	}
	
	//si5345_instance.dump_registers();
	//si5345_instance.test_function();
	printf("DONE!!!!!!!!!!!!!");
	//si5345_instance.dump_registers();
	printf("\n\n\n\n\n\n");
}//endfunction: run_test_input_freq_test



/*! function used to test freerun mode for output 0 on a selectable frequency
arguments: si5345 instance on which to run, desired frequency on output 0
returns: none
*/
void run_test_freerun_out0(si5345 & si5345_instance, uint64_t freq)
{
    uint8_t output_channel = 0;
	si5345_instance.set_registers_freerun();
    
    for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)//disable all outputs
    {
        si5345_instance.enable_output(i, 0);
    }
	si5345_instance.set_frequency(freq, output_channel);
    si5345_instance.set_output_format(output_channel, 4, 0, 0, 0);
    //si5345_instance.enable_output(output_channel, 1);
	//si5345_instance.control_output_en_all(1);
	
	printf("output value = %lu\n", si5345_instance.read_freq_on_out_ch(output_channel) );
}//endfunction: run_test_freerun_out0



/*! function used to test freerun mode for output 0 and 1 on selectable frequencies
arguments: si5345 instance on which to run, freq for output0 and freq for output 1
returns: none
*/
void run_test_freerun_out0_out1(si5345 & si5345_instance, uint64_t freq_out0, uint64_t freq_out1)
{
	si5345_instance.set_registers_freerun();
    si5345_instance.set_frequency(freq_out0, 0);
    for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)
    {
        si5345_instance.enable_output(i, 0);
    }
    si5345_instance.set_output_format(0, 4, 0, 0, 0);
    si5345_instance.set_output_format(1, 4, 0, 0, 0);
    si5345_instance.enable_output(0, 1);
    si5345_instance.set_frequency(freq_out1, 1);
    si5345_instance.enable_output(1, 1);
    
	printf("ch0 : output value = %lu\n",si5345_instance.read_freq_on_out_ch(0) );
	printf("ch1 : output value = %lu\n",si5345_instance.read_freq_on_out_ch(1) );
}//endfunction: run_test_freerun_out0_out1



/*! function used to test freerun mode for output 0, changing its freqency from freq0_out0 to freq1_out0
arguments: si5345 instance on which to run, 2 freq for output0. freq0_out0 => freq1_out0
returns: none
*/
void run_test_freerun_out0_changes(si5345 & si5345_instance, uint64_t freq0_out0, uint64_t freq1_out0)
{
	si5345_instance.set_registers_freerun();
	uint8_t output_channel = 0;
	si5345_instance.set_frequency(freq0_out0, output_channel);
	for(uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)
	{
		si5345_instance.enable_output(i, 0);
	}
	si5345_instance.set_output_format(output_channel, 4, 0, 0, 0);
	si5345_instance.enable_output(output_channel, 1);
	
	printf("output value = %lu\n", si5345_instance.read_freq_on_out_ch(output_channel));
	
	usleep(15 * 1000 * 1000);
	si5345_instance.set_frequency(freq1_out0, output_channel);
	printf("output value = %lu\n", si5345_instance.read_freq_on_out_ch(output_channel));
}//endfunction: run_test_freerun_out0_changes



/*! function used to test freerun mode for all outputs, selecting freqencies between 3 and 12 MHz
arguments: si5345 instance on which to run
returns: none
*/
void run_test_freerun_all_outputs_3M_12M_Hz(si5345 & si5345_instance)
{
	si5345_instance.set_registers_freerun();
	for(int i = 0; i< NR_OUTPUT_CHANNELS; i++)						//set all
	{
        si5345_instance.set_frequency((i+3) * 1000000, i);
	}
	printf("finished set all\n");
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)				//disable all
	{
        si5345_instance.enable_output(i, 0);
	}
	printf("finished disable output all\n");
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)				//output format all
	{
        si5345_instance.set_output_format(i, 4, 0, 0, 0);
	}
	printf("finished output format all\n");
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)				//enable all
	{
        si5345_instance.enable_output(i, 1);
	}
	printf("finished enable output  all\n");
	
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)				//read all
	{
        printf("ch %d: output value = %lu\n",i, si5345_instance.read_freq_on_out_ch(i) );
	}
}//endfunction: run_test_freerun_all_outputs_3M_12M_Hz



/*! function used to test freerun mode for all outputs, selecting freqencies between 10 and 110 KHz
arguments: si5345 instance on which to run
returns: none
*/
void run_test_freerun_all_outputs_10k_110k_Hz(si5345 & si5345_instance)
{
	si5345_instance.set_registers_freerun();
	for(int i = 0; i< NR_OUTPUT_CHANNELS; i++)						//set all
	{
		si5345_instance.set_frequency((i+1) * 10000, i);
	}
	printf("finished set all\n");
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)				//disable all
	{
		si5345_instance.enable_output(i, 0);
	}
	printf("finished disable output all\n");
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)				//output format all
	{
		si5345_instance.set_output_format(i, 4, 0, 0, 0);
	}
	printf("finished output format all\n");
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)				//enable all
	{
		si5345_instance.enable_output(i, 1);
	}
	printf("finished enable output  all\n");
	
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)				//read all
	{
		printf("ch %d: output value = %lu\n",i, si5345_instance.read_freq_on_out_ch(i) );
	}
}//endfunction: run_test_freerun_all_outputs_10k_110k_Hz



/*! function used to read the chip id
arguments: si5345 instance on which to run
returns: none. prints results to console
*/
void run_test_read_id(si5345 & si5345_instance)
{
	std::string temp = si5345_instance.read_id();
	printf("chip id %s\n", temp.c_str() );
}//end function: run_test_read_id



void print_help()
{
#ifndef SI5345_SPI
	printf("\t-a i2c slave address. Must be between [0-127]\n");
	printf("\t-d i2c device\n");
	printf("EXAMPLE: ./main -d /dev/i2c-0 -a 0x68\n");
#else
	printf("\t-d spi device\n");
	printf("EXAMPLE: ./main -d /dev/spidev0.1\n");
#endif
}



	
int main(int argc, char **argv)
{
    int opt;
    int file; 
    char *filepath = NULL;
    unsigned slave_addr = -1;
    
																												// ONLY SPI MODE TESTED
  
	
	while ((opt = getopt(argc, argv, ":d:h")) != -1)
	{
		switch (opt)
		{
			case 'd':
				filepath = new char[strlen(optarg)];
				strcpy(filepath, optarg);
				break;
			case 'h':
				print_help();
				return 0;
			case ':':
				fprintf(stderr,
						"ERROR: -%c option needs a value\n",
						optopt);
				return -EINVAL;
			case '?':
				fprintf(stderr, "unknown option: %c\n", optopt);
				break;
			
		}
	}
	
	
	si5345_interface * interface_0 = new si5345_interface_spi(filepath);
	
    si5345 si5345_instance(interface_0);
    
    run_tests(si5345_instance);
    
	return 0;
}






#endif //si5345_tests___cpp
































