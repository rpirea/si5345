
#ifndef si5345_tests___h
#define si5345_tests___h

#include <cstdint>



//general device 
static const uint8_t NR_INPUT_CHANNELS = 4;
static const uint8_t NR_OUTPUT_CHANNELS = 10;
static const uint8_t NR_MULTISYNTHS = 5;



//general tests 
static const int SEED = 1;





void run_tests(si5345 & si5345_instance);

//void read_all_regs(si5345 si5345_instance);

void run_test_manual_test(si5345 & si5345_instance);

void run_test_freerun_out0(si5345 & si5345_instance, uint64_t freq);

void run_test_freerun_out0_out1(si5345 & si5345_instance, uint64_t freq_out0, uint64_t freq_out1);

void run_test_freerun_out0_changes(si5345 & si5345_instance, uint64_t freq0_out0, uint64_t freq1_out0);

void run_test_freerun_all_outputs_3M_12M_Hz(si5345 & si5345_instance);

void run_test_freerun_all_outputs_10k_110k_Hz(si5345 & si5345_instance);

void run_test_read_id(si5345 & si5345_instance);

void run_test_input_and_output_frequencies(si5345 & si5345_instance);

void run_test_input_freq_test(si5345 & si5345_instance);

void run_test_final_test(si5345 & si5345_instance);

void print_help();

int main(int argc, char **argv);



#endif //si5345_tests___h




















































