//
// Created by rpirea on 3/5/20.
//

#ifndef CERN_DRIVER_SI5345_SI5345_INTERFACE_SPI_HPP
#define CERN_DRIVER_SI5345_SI5345_INTERFACE_SPI_HPP

#include "si5345_interface.hpp"

#include <spidev/spidev.hpp>

#include <string>

class si5345_interface_spi : public si5345_interface {

    spidev *spi_dev;

public:
    virtual uint8_t read_register_raw(uint8_t reg_addr);

    virtual uint8_t read_register(uint16_t reg_addr);

    virtual void set_page(uint8_t page);

    virtual void write_register(uint16_t reg_addr, uint8_t value);

    si5345_interface_spi(std::string spi_device);
};


#endif //CERN_DRIVER_SI5345_SI5345_INTERFACE_SPI_HPP
