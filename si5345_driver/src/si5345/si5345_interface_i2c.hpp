//
// Created by rpirea on 3/5/20.
//

#ifndef CERN_DRIVER_SI5345_SI5345_INTERFACE_I2C_HPP
#define CERN_DRIVER_SI5345_SI5345_INTERFACE_I2C_HPP

#include "si5345_interface.hpp"

#include <string>

class si5345_interface_i2c : public si5345_interface {

    int i2c_dev;

public:
    virtual uint8_t read_register_raw(uint8_t reg_addr);

    virtual uint8_t read_register(uint16_t reg_addr);

    virtual void set_page(uint8_t page);

    virtual void write_register(uint16_t reg_addr, uint8_t value);

    si5345_interface_i2c(const std::string &i2c_device, uint8_t i2c_address);

};

#endif //CERN_DRIVER_SI5345_SI5345_INTERFACE_I2C_HPP
