#ifndef si5345___h
#define si5345___h

#include <spidev/spidev.hpp>
#include <i2c/smbus.h>

#include <cstdint>
#include <map>
#include <vector>
#include <utility>
#include <stdint.h>

#include "si5345_interface.hpp"





typedef struct
{
	unsigned int address; /* 16-bit register address */
	unsigned char value; /* 8-bit register data */
	
} si5345_revd_register_t;



class si5345 
{
private:

    si5345_interface *mInterface;
	
    const static uint64_t default_dspll_out_freq = 13.3*1000*1000*1000;
    const static uint64_t default_xtal_freq = 48*1000*1000;
    
    uint64_t mDSPLLOutFrequency;
    uint64_t mXtalFreq;
    uint64_t mInputChannel_freq[4];

public:
	
	virtual void dump_registers();
	void set_registers_input_controlled();
	void set_registers_freerun();
	virtual void test_function();
	
	si5345(si5345_interface *si5345Interface);
    si5345(si5345_interface *si5345Interface, uint64_t xtal_freq);
    si5345(si5345_interface *si5345Interface, uint64_t xtal_freq, uint64_t vco_freq);
    virtual std::string read_id();
    virtual void reset_hard();
    virtual void reset_soft_all();
    virtual void reset_soft();
    virtual void set_output_format(uint8_t output_channel, uint8_t format, uint8_t out_sync, uint8_t disable_state, uint8_t cmos_drv);
    virtual void enable_output(uint8_t output_channel, uint8_t command);
    virtual void set_frequency(uint64_t desired_frequency, uint8_t output_channel);
    virtual uint64_t read_freq_on_out_ch(uint8_t output_channel);
    virtual void select_input(uint8_t input_channel, uint64_t input_freq);
    virtual void set_input_enable(uint8_t input_channel, uint8_t value);
    virtual void set_input_format(uint8_t input_channel, uint8_t format);
    
    
private:

    virtual void init_dspll();
	virtual void enable_freerun_mode();
    virtual uint64_t read_register_by_size(uint16_t reg_addr, uint8_t size);
    virtual void write_register_by_size(uint16_t reg_addr, uint8_t size, uint64_t value);
	
    
    
    // getters/setters
    virtual uint64_t get_dspll_out_freq();
    virtual uint64_t get_xtal_freq();
    virtual uint64_t get_in_freq();
    virtual void set_DSPLL_out_freq(uint64_t freq);
    virtual void set_XTAL_freq(uint64_t freq);
    
	
    
    //preamble + postable for dynamic pll changes
    virtual void preamble_reg_modifications();
    virtual void postamble_reg_modifications();

    
    
    //Mxaxb functions
    virtual void set_MXAXB_numerator(uint64_t numerator);
    virtual void set_MXAXB_denominator(uint32_t denominator);
    virtual void set_MXAXB(uint64_t numerator, uint32_t denominator);
    virtual void set_MXAXB_fractional_mode(uint8_t command);
    virtual void set_MXAXB(double value);
    virtual uint64_t read_reg_MXAXB_numerator();
    virtual uint32_t read_reg_MXAXB_denominator();
    virtual double read_MXAXB();
    virtual void update_MXAXB();

    
    
    //Pxaxb functions
    virtual void set_pxapxb(uint8_t value);
    virtual uint8_t read_reg_pxapxb();
    virtual uint8_t read_divider_pxaxb();

    
    
    //P functions
    void set_P_fractional_mode(uint8_t input_channel, uint8_t command);
    void update_P_values(uint8_t input_channel);
    void set_divider_P_numerator(uint8_t input_channel, uint64_t numerator);
    uint64_t read_reg_divider_P_numerator(uint8_t input_channel);
    void set_divider_P_denominator(uint8_t input_channel, uint32_t denominator);
    uint32_t read_reg_divider_P_denominator(uint8_t input_channel);
    void set_divider_P(uint8_t input_channel, uint64_t numerator, uint32_t denominator);
    void set_divider_P(uint8_t input_channel, double value);
    double read_divider_P(uint8_t input_channel);
    
    
    
    //M functions
    void set_M_numerator(uint64_t numerator);
    void set_M_denominator(uint32_t denominator);
    void update_M();
    void set_M_fractional_mode(uint8_t command);
    void set_M(uint64_t numerator,uint32_t denominator);
    void set_M(double value);
    uint64_t read_reg_M_numerator();
    uint32_t read_reg_M_denominator();
    double read_divider_M();
    
    
    
    //N functions
    virtual void update_N_values(uint8_t update_all, uint8_t multisynth);
    virtual void set_divider_N_numerator(uint8_t multisynth, uint64_t numerator);
    virtual uint64_t read_reg_divider_N_numerator(uint8_t multisynth);
    virtual void set_divider_N_denominator(uint8_t multisynth, uint32_t denominator);
    virtual uint32_t read_reg_divider_N_denominator(uint8_t multisynth);
    virtual void set_multisynth_divider(uint8_t multisynth, uint64_t numerator, uint32_t denominator);
    virtual void set_multisynth_divider(uint8_t multisynth, double divider_N);
    virtual double read_divider_N(uint8_t multisynth);
    virtual void set_N_fractional_mode(uint8_t multisynth, uint8_t command);
    virtual uint8_t read_N_fractional_mode(uint8_t multisynth);

    
    
    //R functions
    virtual void set_OUT0_RDIV_FORCE2(uint8_t output_channel, uint8_t value);
    virtual uint8_t read_reg_OUT0_RDIV_FORCE2(uint8_t output_channel);
    virtual void set_output_divider(uint8_t output_channel, uint32_t denominator);
    virtual uint32_t read_divider_RX_REG(uint8_t output_channel);
    virtual uint32_t read_divider_RX_denominator(uint8_t output_channel);
    virtual double read_divider_R(uint8_t output_channel);

	
    
    //general functions
	virtual void write_reg_input_clock_select_in_sel(uint8_t input_channel);
    virtual uint8_t read_reg_input_clock_select_in_sel();
	virtual void set_input_reference_regs(uint8_t input_channel, uint64_t input_freq);
	virtual void set_input_los_trigger_threshold_regs(uint8_t input_channel, uint64_t input_freq);
	virtual void set_input_clear_trigger_threshold_regs(uint8_t input_channel, uint64_t input_freq);
	virtual void set_input_fixed_regs(uint8_t input_channel, uint64_t input_freq);
	virtual void set_input_HOLD_15M_CYC_COUNT_PLLB_regs(uint64_t input_freq);
	virtual void set_input_HSW_COARSE_PM_LEN_regs(uint64_t input_freq);
	virtual void set_input_PFD_EN_DELAY_regs(uint64_t input_freq);
	virtual void set_input_HOLDEXIT_BW5_regs(uint64_t input_freq);
	virtual void set_input_REFCLK_HYS_SEL_regs(uint64_t input_freq);
    virtual int8_t find_empty_multysinth(std::vector<int8_t> out_ch_en, std::map<int8_t, int8_t> output_ch_multisynth_connections);
    virtual int8_t find_single_connected_multysinth(std::vector<int8_t> out_ch_en, std::map<int8_t, int8_t> output_ch_multisynth_connections);
    virtual std::vector<int8_t> find_output_channels_connected_to_multysinth(int8_t multysinth, std::vector<int8_t> out_ch_en, std::map<int8_t, int8_t> output_ch_multisynth_connections);
    virtual uint64_t gcd(uint64_t a, uint64_t b);
    virtual uint64_t lcm(uint64_t a, uint64_t b);
    virtual std::pair<uint64_t, uint64_t> create_fraction(double target_value, uint8_t max_bits_numerator, uint8_t max_bits_denominator);
    virtual void control_output_en_all(uint8_t command);
    virtual uint8_t read_reg_output_en_all();
    virtual uint8_t read_reg_output_en(uint8_t output_channel);
    virtual int check_enable_output_channel(uint8_t output_channel);
    virtual void select_multisynth_for_output_channel(uint8_t multisynth, uint8_t output_channel);
    virtual uint8_t read_multisynth_for_output_channel(uint8_t output_channel);
    virtual std::map<int8_t, int8_t> read_connections_output_channels_multisynths();
    virtual uint64_t read_freq_on_output_channel_no_disable(uint8_t output_channel);
    virtual void set_fastlock_enable();

    /*
    //unused currently

    //other functions
    void set_bw_pll(uint8_t bw_pll_regs[6]);
    void set_fast_bw_pll(uint8_t fast_bw_pll_regs[6]);
    void set_holdexit_bw(uint8_t fast_bw_pll_regs[6]);
    void set_hold_15m_cyc_count_pllb(uint32_t value);
    void enable_freerun_mode();
    uint8_t check_holdover_freerun();
    void force_holdover(uint8_t command);
    uint8_t read_force_holdover();
    void set_vdd(uint8_t output_channel, uint8_t vdd_value);
    //*/
};








#endif //si5345___h





















