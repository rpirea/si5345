//
// Created by rpirea on 3/5/20.
//

#ifndef CERN_DRIVER_SI5345_SI5345_INTERFACE_HPP
#define CERN_DRIVER_SI5345_SI5345_INTERFACE_HPP

#include <cstdint>

class si5345_interface {
public:
    virtual uint8_t read_register_raw(uint8_t reg_addr) = 0;

    virtual uint8_t read_register(uint16_t reg_addr)  = 0;

    virtual void set_page(uint8_t page)  = 0;

    virtual void write_register(uint16_t reg_addr, uint8_t value) = 0;

};


#endif //CERN_DRIVER_SI5345_SI5345_INTERFACE_HPP
