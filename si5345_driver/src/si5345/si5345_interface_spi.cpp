//
// Created by rpirea on 3/5/20.
//

#define REG_PAGE 0x01
#define SI5345_SPI_SET_ADDRESS 0x00
#define SI5345_SPI_WRITE_DATA 0x40
#define SI5345_SPI_READ_DATA 0x80

#include "si5345_interface_spi.hpp"

#include <unistd.h>


si5345_interface_spi::si5345_interface_spi(std::string spi_device) {
    spidev_config si5345_config;
    si5345_config.config = 0;
    spi_dev = new spidev(spi_device, si5345_config, 1000000);
}

uint8_t si5345_interface_spi::read_register_raw(uint8_t reg_addr) {
    uint8_t ret;
    uint8_t offset = reg_addr;
    uint8_t read_register_buff[3];
    read_register_buff[0] = SI5345_SPI_SET_ADDRESS;
    read_register_buff[1] = offset;
    read_register_buff[2] = SI5345_SPI_READ_DATA;

    spi_dev->transfer(read_register_buff, &ret, sizeof(read_register_buff), sizeof(ret));

    return ret;
}

uint8_t si5345_interface_spi::read_register(uint16_t reg_addr) {
    uint8_t ret;
    uint8_t page = reg_addr >> 8;
    uint8_t offset = reg_addr;
    uint8_t read_register_buff[3];
    read_register_buff[0] = SI5345_SPI_SET_ADDRESS;
    read_register_buff[1] = offset;
    read_register_buff[2] = SI5345_SPI_READ_DATA;

    set_page(page);
    spi_dev->transfer(read_register_buff, &ret, sizeof(read_register_buff), sizeof(ret));
    usleep(4);
    return ret;
}

void si5345_interface_spi::set_page(uint8_t page) {
    uint8_t set_page_buff[4];
    set_page_buff[0] = SI5345_SPI_SET_ADDRESS;
    set_page_buff[1] = REG_PAGE;
    set_page_buff[2] = SI5345_SPI_WRITE_DATA;
    set_page_buff[3] = page;
    spi_dev->write(set_page_buff, sizeof(set_page_buff));
    usleep(4);
}

void si5345_interface_spi::write_register(uint16_t reg_addr, uint8_t value) {
    uint8_t page = reg_addr >> 8;
    uint8_t offset = reg_addr;
    uint8_t read_register_buff[4];
    read_register_buff[0] = SI5345_SPI_SET_ADDRESS;
    read_register_buff[1] = offset;
    read_register_buff[2] = SI5345_SPI_WRITE_DATA;
    read_register_buff[3] = value;

    set_page(page);
    spi_dev->write(read_register_buff, sizeof(read_register_buff));
    usleep(4);
}