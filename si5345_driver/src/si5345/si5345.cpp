
#ifndef si5345___cpp
#define si5345___cpp

#include "si5345_defines.hpp"
#include "si5345.hpp"

#include <i2c/smbus.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <cassert>
#include <cerrno>
#include <cstdint>
#include <cstring>
#include <map>
#include <vector>
#include <cmath>

using namespace std;





/*! function used to test functionality. used in debugging \n
arguments: none \n
returns: none \n
documentation: none
*/
void si5345::test_function()
{
	/*
	uint64_t p = read_divider_P(0);
	uint64_t m = read_divider_M();
	
	uint64_t selected_channel = read_reg_input_clock_select_in_sel();
	
	uint64_t p_num = read_reg_divider_P_numerator(0);
	uint64_t p_denom = read_reg_divider_P_denominator(0);
	
	uint64_t freq = 3000000;
	
	map<int8_t, int8_t> connection_r_n = read_connections_output_channels_multisynths();
	
	vector<uint64_t> out_freq;
	
	uint8_t i;
	for(i = 0; i < NR_OUTPUT_CHANNELS; i++)
	{
		out_freq.push_back(read_freq_on_out_ch(i));
	}
	
	vector<double> dividers_N;
	for(i =0;i<NR_MULTISYNTHS;i++)
	{
		dividers_N.push_back(read_divider_N(i));
	}
	
	vector<double> dividers_R;
	for(i =0;i<NR_OUTPUT_CHANNELS;i++)
	{
		dividers_R.push_back(read_divider_R(i));
	}
	
	vector<uint8_t> output_enables;
	for(i =0;i<NR_OUTPUT_CHANNELS;i++)
	{
		output_enables.push_back(read_reg_output_en(i));
	}
	
	enable_output(0,1);
	enable_output(1,1);
	enable_output(2,1);
	
	//dump_registers();
	//*/
	return;
}//endfunction: test_function



/*! function used to print out all registers \n
arguments: none \n
returns: none \n
documentation: none \n
*/
void si5345::dump_registers()
{
	for (uint16_t i = 0; i < 0x0B58; i++)
	{
		uint8_t data_read = mInterface->read_register(i);
		printf("addr = %x data = %x\n", i, data_read);
	}
}//endfunction: dump_register



/*! constructor \n
arguments: spi interface on which data is sent \n
returns: none \n
documentation: none \n
*/
si5345::si5345(si5345_interface *si5345Interface) : si5345(si5345Interface,default_xtal_freq,default_dspll_out_freq)
{

}//endfunction: si5345
/*! constructor \n
arguments: spi interface on which data is sent, xtal frequency - usually 48MHz \n
returns: none \n
documentation: none \n
*/
si5345::si5345(si5345_interface *si5345Interface, uint64_t xtal_freq) : si5345(si5345Interface,xtal_freq,default_dspll_out_freq)
{

}//endfunction: si5345
/*! constructor  \n
arguments: spi interface on which data is sent, xtal frequency - usually 48MHz, vco freq - the dspll out freq \n
returns: none \n
documentation: none \n
*/
si5345::si5345(si5345_interface *si5345Interface, uint64_t xtal_freq, uint64_t vco_freq)
{
	mInterface = si5345Interface;
	mXtalFreq = xtal_freq;
	mDSPLLOutFrequency = vco_freq;
	init_dspll();
}//endfunction: si5345



/*!  function called to set the initial default settings of the si5345 \n
function called by initial_settings_default() \n
sets XTAL freq to 48MHz \n
sets XTAL freq to 13GHz \n
arguments: none \n
returns: none \n
documentation: none
*/
void si5345::init_dspll()
{
	for(uint8_t i = 0; i < NR_INPUT_CHANNELS; i++)  //unused
	{
		set_divider_P(i,1);
	}
	control_output_en_all(0);
	set_XTAL_freq(mXtalFreq);//48M
	set_DSPLL_out_freq(mDSPLLOutFrequency);
	for (uint8_t i = 0; i < NR_OUTPUT_CHANNELS; i++) //disable all outputs
	{
		enable_output(i, 0);
	}
	
	enable_freerun_mode();
	
}//endfunction: init_dspll



// getters/setters
/*!  function used to return the currently set mDSPLLOutFrequency (Fvco) (from class, not read from device)  \n
arguments: none \n
returns: currently set mDSPLLOutFrequency \n
documentation : none \n
*/
uint64_t si5345::get_dspll_out_freq()
{
	return mDSPLLOutFrequency;
}//endfunction: get_dspll_out_freq



/*!  function used to return the currently set mXtalFreq (from class, not device)  \n
arguments: none \n
returns: currently set mXtalFreq \n
 documentation :  none  \n
*/
uint64_t si5345::get_xtal_freq()
{
	return mXtalFreq;
}//endfunction: get_xtal_freq



/*!  function used to return the currently set input frequency \n (from class, not device)
arguments: none \n
returns: currently set input freqency \n
documentation: none \n
*/
uint64_t si5345::get_in_freq()
{
	uint8_t selected_channel = read_reg_input_clock_select_in_sel();
	
	return mInputChannel_freq[selected_channel];
}//endfunction: get_in_freq



/*!   function used to calculate and set dividers in order to have the desired freqency at the DSPLL out (Fvco) \n
arguments: uint64_t freq: dspll out frequency that is to be set \n
returns: none \n
documentation : reference manual + clock builder pro  \n
*/
void si5345::set_DSPLL_out_freq(uint64_t freq)
{
	assert(freq >= 13000000000);
	assert(freq <= 14000000000);
	
	mDSPLLOutFrequency = freq;
	
	double P_divider = 1;
	double input_freq = mInputChannel_freq[read_reg_input_clock_select_in_sel()];
	if(input_freq == 0)
	{
		input_freq = 1; // avoid div by 0; used for initialize
	}
	uint64_t dspll_in_freq = input_freq / P_divider; // route input
	double dspll_in_freq_route_XAXB = 1.0 * mXtalFreq / read_divider_pxaxb();
	
	
	
	double M_divider = (mDSPLLOutFrequency*1.0) / (dspll_in_freq * 5);
	double mxaxb = 1.0 * mDSPLLOutFrequency / dspll_in_freq_route_XAXB;
	
	set_M(M_divider);
	set_MXAXB(mxaxb);
}//endfunction: set_DSPLL_out_freq



/*!   function used to set a new XTAL frequency and calculate coresponing Mxaxb divider \n
arguments: uint64_t freq: xtal frequency that is to be set \n
returns: none \n
documentation reference manual page 9 + 100 + 102 \n
*/
void si5345::set_XTAL_freq(uint64_t freq)
{
	mXtalFreq = freq;
	set_pxapxb(0);
	double mxaxb = 1.0 * mDSPLLOutFrequency / freq * 1;
	set_MXAXB(mxaxb);
}//endfunction: set_XTAL_freq



/*!   function used to configure the OOF ratio for reference + oof divider select \n
arguments:  input_channel=what input channel to be used for synchronization \n
            input_freq=input freq on selected channel. used to calculate dividers \n
documentation: reference manual page 90 and 92 \n
*/
void si5345::set_input_reference_regs(uint8_t input_channel, uint64_t input_freq)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	mInterface->write_register(REG_INPUT_OOF_REFERENCE_SELECT,4);//select xaxb
	
	// compute registers values:
		// y = 1431.65575823847 * x - 676.114388804059 ; from clock builder pro and excell trendline
	unsigned long long int initial_value;
	
	initial_value = 1431.65575823847 * input_freq - 676.114388804059;

	unsigned long long int temp_value = initial_value;
	long double temp_value_double = temp_value;
	int divider=-1;// after first while, divider should be 0
	
	while( (temp_value == temp_value_double) && (temp_value != 0) )	// compute divider
	{
		temp_value_double = temp_value_double / 2;
		temp_value = temp_value / 2;
		divider = divider + 1;
	}
	
	unsigned long long int oof_ratio_for_reference = initial_value;
	for(int i = 0; i < divider; i++)	//compute oof for reference
	{
		oof_ratio_for_reference = oof_ratio_for_reference / 2;
	}
	unsigned long long int oof_ratio_limit = 1;		//make sure it fits in 26 bits
	oof_ratio_limit = oof_ratio_limit << REG_OOF_DIVIDER_SELECT_SIZE_b;
	while(oof_ratio_for_reference > oof_ratio_limit)
	{
		oof_ratio_for_reference = oof_ratio_for_reference / 2;
		divider = divider + 1;
	}
	
	uint16_t addr = REG_OOF_DIVIDER_SELECT_BASE + (input_channel * REG_OOF_DIVIDER_SELECT_OFFSET) ;
	mInterface->write_register(addr ,divider);
	addr =  REG_OOF_RATIO_REF_BASE + (input_channel * REG_OOF_RATIO_REF_OFFSET) ;
	write_register_by_size(addr, REG_OOF_DIVIDER_SELECT_SIZE_B, oof_ratio_for_reference);
}//endfunction: set_input_reference_regs



/*! function used to set the los trigger threshold \n
arguments: input channel for which the threshold is set, lock frequency desired used to comput reg value \n
returns: none \n
documentation: RM page 88 \n
*/
void si5345::set_input_los_trigger_threshold_regs(uint8_t input_channel, uint64_t input_freq)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	uint16_t value =0;
	if(input_freq >=1500000)
	{
		value = 54;
	}
	else
	{
		//y = 40977260*x^-0.970805 +20
		value = 20 + 40977260*(pow(input_freq,-0.970805));
		if (value > pow(2,16))
		{
			value = pow(2,16) -1 ;
		}
	}
	
	uint16_t addr = REG_LOS_TRIGGER_THRESHOLD_BASE + (input_channel * REG_LOS_TRIGGER_THRESHOLD_OFFSET) ;
	write_register_by_size(addr, 2, value);
}//endfunction: set_input_los_trigger_threshold_regs



/*! function used to set the los clear threshold \n
arguments: input channel for which the threshold is set, lock frequency desired used to comput reg value \n
returns: none \n
documentation: RM page 89 \n
*/
void si5345::set_input_clear_trigger_threshold_regs(uint8_t input_channel, uint64_t input_freq)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	uint16_t value =0;
	if(input_freq >=1500000)
	{
		value = 54;
	}
	else
	{
		//y = 40977260*x^-0.970805 +20
		value = 20 + 40977260*(pow(input_freq,-0.970805));
		if (value > pow(2,16))
		{
			value = pow(2,16) -1 ;
		}
	}
	
	uint16_t addr = REG_CLEAR_TRIGGER_THRESHOLD_BASE + (input_channel * REG_CLEAR_TRIGGER_THRESHOLD_OFFSET) ;
	write_register_by_size(addr, 2, value);
}//endfunction: set_input_clear_trigger_threshold_regs



/*! function used to set the registers that have fixed values. hardcoded \n
arguments: input channel for which the threshold is set, lock frequency desired used to comput reg value \n
returns: none \n
documentation: RM misc pages 80-125 \n
*/
void si5345::set_input_fixed_regs(uint8_t input_channel, uint64_t input_freq)
{
	uint16_t addr;
	uint64_t value;
	
	addr = 0x009D;	//slow lol detection
	value = 8;
	write_register_by_size(addr , 1, value);
	
	addr = 0x00A9;	//LOL_CLR_DELAY_DIV256
	value = 12770;
	write_register_by_size(addr , 4, value);
	
	addr = 0x00EA;	//fastlock extend
	value = 24586;
	write_register_by_size(addr , 4, value);
	
	addr = 0x0294;	//fastlock extend scale
	value = 0xB0;
	write_register_by_size(addr , 1, value);
	
	addr = 0x029D;	//fastlock delay on lol
	value = 0x0001FA;
	write_register_by_size(addr , 3, value);
	
	addr = 0x02A9;	//fastlock delay on input switch
	value = 0x04CC;
	write_register_by_size(addr , 3, value);
	
	addr = 0x05A1;	//HOLDEXIT_BW4
	value = 7;
	write_register_by_size(addr , 1, value);
	
}//endfunction: set_input_HOLD_15M_CYC_COUNT_PLLB_regs



/*! function used to set the HOLD_15M_CYC_COUNT_PLLB register \n
arguments:lock frequency desired used to comput reg value \n
returns: none \n
documentation: RM page 114 \n
*/
void si5345::set_input_HOLD_15M_CYC_COUNT_PLLB_regs(uint64_t input_freq)
{
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	uint32_t value =0;
	if(input_freq >=1500000)
	{
		value = 834;
	}
	else
	{
		//y = 1668986000*x^-1.000147 + 10
		value = 10 + 1668986000*(pow(input_freq,-1.000147));
		if (value > pow(2,24))
		{
			value = pow(2,24) -1 ;
		}
	}
	
	uint16_t addr = REG_HOLD_15M_CYC_COUNT_PLLB;
	write_register_by_size(addr, 3, value);
}//endfunction: set_input_HOLD_15M_CYC_COUNT_PLLB_regs



/*! function used to set the HSW_COARSE_PM_LEN register \n
arguments:lock frequency desired used to comput reg value \n
returns: none \n
documentation: RM page 116 \n
*/
void si5345::set_input_HSW_COARSE_PM_LEN_regs(uint64_t input_freq)
{														//if needed. finer tuning can be done
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	uint32_t value =0;
	if(input_freq <= 25000)
	{
		value = 10;
	}
	else
	{
		if(input_freq <= 45000)
		{
			value = 11;
		}
		else
		{
			if(input_freq <= 75000)
			{
				value = 12;
			}
			else
			{
				if(input_freq <= 150000)
				{
					value = 13;
				}
				else
				{
					if(input_freq <= 400000)
					{
						value = 14;
					}
					else
					{
						if(input_freq <= 850000)
						{
							value = 15;
						}
						else
						{
							if(input_freq <= 1500000)
							{
								value = 16;
							}
							else
							{
								value = 17;
							}
						}
					}
				}
			}
		}
	}
	
	uint16_t addr = REG_HSW_COARSE_PM_LEN;
	write_register_by_size(addr, 1, value);
}//endfunction: set_input_HSW_COARSE_PM_LEN_regs



/*! function used to set the PFD_EN_DELAY register \n
arguments:lock frequency desired used to comput reg value \n
returns: none \n
documentation: RM page 117 \n
*/
void si5345::set_input_PFD_EN_DELAY_regs(uint64_t input_freq)
{
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	uint32_t value =0;
	if(input_freq >=1500000)
	{
		value = 13;
	}
	else
	{
		//y = 12514777.264763 * ( x^ -0.931909984577044 )			// not the best approximation
		value = 10 + 12514777.264763*(pow(input_freq,-0.931909984577044));
		if(value > 4000)
    	{
			value = 4000;
    	}
	}
	
	uint16_t addr = REG_PFD_EN_DELAY;
	write_register_by_size(addr, 2, value);
}//endfunction: set_input_PFD_EN_DELAY_regs



/*! function used to set the HOLDEXIT_BW5 register \n
arguments:lock frequency desired used to comput reg value \n
returns: none \n
documentation: RM page 118 \n
*/
void si5345::set_input_HOLDEXIT_BW5_regs(uint64_t input_freq)
{														//if needed. finer tuning can be done
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	uint32_t value =0;
	if(input_freq <= 50000)
	{
		value = 1;
	}
	else
	{
		if(input_freq <= 450000)
		{
			value = 7;
		}
		else
		{
			value = 63;
		}
	}
	uint16_t addr = REG_HOLDEXIT_BW5;
	write_register_by_size(addr, 1, value);
}//endfunction: set_input_HOLDEXIT_BW5_regs



/*! function used to set the REFCLK_HYS_SEL register \n
arguments:lock frequency desired used to comput reg value \n
returns: none \n
documentation: RM page 120 \n
*/
void si5345::set_input_REFCLK_HYS_SEL_regs(uint64_t input_freq)
{														//if needed. finer tuning can be done
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	uint32_t value =0;
	if(input_freq <= 750000)
	{
		value = 586;
	}
	else
	{
		value = 585;
	}
	uint16_t addr = REG_REFCLK_HYS_SEL;
	write_register_by_size(addr, 2, value);
}//endfunction: set_input_REFCLK_HYS_SEL_regs



/*!   function used to select which input channel is used to synchronize the output clocks \n
used for manual select. also calculates and sets the Pdivider and M divider in order to obtain \n
the proper (currently selected) DSPLL out frequency \n
arguments:  input_channel=what input channel to be used for synchronization \n
            input_freq=input freq on selected channel. used to calculate dividers \n
documentation: reference manual page 113 \n
*/
void si5345::select_input(uint8_t input_channel, uint64_t input_freq)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(input_freq >= INPUT_MIN_FREQ);
	assert(input_freq <= INPUT_MAX_FREQ);
	
	mInputChannel_freq[input_channel] = input_freq;
	
	control_output_en_all(0);
	double P_divider = 1;
	
	uint64_t dspll_in_freq = input_freq / P_divider;
	
	set_input_enable(input_channel,1);
	
	write_reg_input_clock_select_in_sel(input_channel);
	
	set_P_fractional_mode(input_channel, 0);
	
	set_divider_P(input_channel, P_divider);
	
	uint64_t M_divider = round((mDSPLLOutFrequency*1.0) / (dspll_in_freq * 5)); // should be double.
								// does not work with fractional m divider.
	set_M(M_divider);
	
	set_input_reference_regs(input_channel, input_freq);	//2m
	set_input_REFCLK_HYS_SEL_regs( input_freq);
	set_input_HOLDEXIT_BW5_regs( input_freq);
	set_input_PFD_EN_DELAY_regs( input_freq);
	set_input_HSW_COARSE_PM_LEN_regs( input_freq);
	set_input_HOLD_15M_CYC_COUNT_PLLB_regs( input_freq);
	set_input_fixed_regs( input_channel,  input_freq);
	set_input_clear_trigger_threshold_regs( input_channel,  input_freq);
	set_input_los_trigger_threshold_regs( input_channel,  input_freq);
	
	control_output_en_all(1);
}//endfunction: select_input



/*!   function used to enable/disable a particular input channel \n
arguments:  input_channel=what input channel is enabled/disabled \n
            value= 0==disable, 1==enable \n
documentation: reference manual page 120 \n
*/
void si5345::set_input_enable(uint8_t input_channel, uint8_t value)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(value <= 1);
	
	uint8_t in_en_current = mInterface->read_register(REG_IN_EN);
	//in_en_current = in_en_current & (~( 1<< (input_channel + REG_IN_EN_POSITION)) );
	uint8_t data = value << (input_channel + REG_IN_EN_POSITION);
	data = in_en_current | data;
	
	mInterface->write_register(REG_IN_EN,data);
	set_fastlock_enable();
	
}//endfunction: set_input_enable



/*!   function used to enable fastlock aquisition \n
arguments:  none \n
documentation: reference manual page 111 and 11 \n
*/
void si5345::set_fastlock_enable()
{
	mInterface->write_register(REG_FAST_LOCK_CTRL,3);
}//endfunction: set_fastlock_enable



//preamble + postable for dynamic pll changes
/*!   function used to write all necessary registers before making dynamic pll changes \n
arguments: none \n
returns: none \n
documentation: reference manual page 14 \n
*/
void si5345::preamble_reg_modifications()
{
	mInterface->write_register(0x0B24, 0xC0);
	mInterface->write_register(0x0B25, 0x00);
	mInterface->write_register(0x0540, 0x01);
	usleep(400 * 1000);
}//endfunction: preamble_reg_modifications



/*!   function used to write all necessary register after making dynamic pll changes \n
arguments: none \n
returns: none \n
documentation: reference manual page 14 \n
*/
void si5345::postamble_reg_modifications()
{
	mInterface->write_register(0x0514, 0x01);
	mInterface->write_register(0x001C, 0x01);
	mInterface->write_register(0x0540, 0x00);
	mInterface->write_register(0x0B24, 0xC3);
	mInterface->write_register(0x0B25, 0x02);
}//endfunction: postamble_reg_modifications





//Mxaxb functions

/*!   function used to set the Mxaxb divider numerator \n
does not update value \n
arguments: uint64_t numerator on 44b \n
returns: none \n
documentation: RM page 103 \n
*/
void si5345::set_MXAXB_numerator(uint64_t numerator)
{
	assert(numerator < ((uint64_t) 1 << REG_MXAXB_DIV_NUMERATOR_SIZE_b));
	
	preamble_reg_modifications();
	write_register_by_size(REG_MXAXB_DIV_NUMERATOR_BASE, REG_MXAXB_DIV_NUMERATOR_SIZE_B, numerator);
	postamble_reg_modifications();
}//endfunction: set_MXAXB_numerator



/*!   function used to set the Mxaxb divider numerator \n
does not update value \n
arguments: uint64_t denominator on 32b \n
returns: none \n
documentation: RM page 103 \n
*/
void si5345::set_MXAXB_denominator(uint32_t denominator)
{
	assert(denominator < ((uint64_t) 1 << REG_MXAXB_DIV_DENOMINATOR_SIZE_b));
	
	preamble_reg_modifications();
	write_register_by_size(REG_MXAXB_DIV_DENOMINATOR_BASE, REG_MXAXB_DIV_DENOMINATOR_SIZE_B, denominator);
	postamble_reg_modifications();
}//endfunction: set_MXAXB_denominator



/*!   function used to set the Mxaxb divider \n
also updates registers \n
arguments: uint64_t numerator on 44b ,  uint64_t denominator on 32b \n
returns: none \n
documentation: RM page 103 \n
*/
void si5345::set_MXAXB(uint64_t numerator, uint32_t denominator)
{
	set_MXAXB_numerator(numerator);
	set_MXAXB_denominator(denominator);
	update_MXAXB();
}//endfunction: set_MXAXB



/*!   function used to set the Mxaxb fraction/integer mode \n
arguments: command: 1 => integer; 0 => fractional mode; \n
returns: none \n
documentation: RM page 121 \n
*/
void si5345::set_MXAXB_fractional_mode(uint8_t command)
{
	assert(command < 2);
	preamble_reg_modifications();
	mInterface->write_register(REG_MXAXB_FRACTIONAL_MODE, command);
	postamble_reg_modifications();
}//endfunction: set_MXAXB_fractional_mode



/*!   function used to set the Mxaxb divider \n
computes numerator + denominator and also updates registers \n
sets fractional mode accordingly \n
arguments: double value : divider value \n
returns: none \n
documentation: RM page 103 \n
*/
void si5345::set_MXAXB(double value)
{
	if (value == (uint64_t) value)
	{
		set_MXAXB_fractional_mode(1);//integer mode
	}
	else
	{
		set_MXAXB_fractional_mode(0);//fractional mode
	}
	
	pair<uint64_t, uint64_t> MXAXB = create_fraction(value, REG_MXAXB_DIV_NUMERATOR_SIZE_b, REG_MXAXB_DIV_DENOMINATOR_SIZE_b);
	
	set_MXAXB(MXAXB.first, MXAXB.second);
}//endfunction: set_MXAXB



/*!   function used to read the Mxaxb divider numerator \n
arguments: none \n
returns: 44b Mxaxb numerator value \n
documentation: RM page 103 \n
*/
uint64_t si5345::read_reg_MXAXB_numerator()
{
	uint64_t data = read_register_by_size(REG_MXAXB_DIV_NUMERATOR_BASE, REG_MXAXB_DIV_NUMERATOR_SIZE_B);
	
	return data;
}//endfunction: read_reg_MXAXB_numerator



/*!   function used to read the Mxaxb divider denominator \n
arguments: none \n
returns: 32b Mxaxb denominator value \n
documentation: RM page 103 \n
*/
uint32_t si5345::read_reg_MXAXB_denominator()
{
	uint64_t data = read_register_by_size(REG_MXAXB_DIV_DENOMINATOR_BASE, REG_MXAXB_DIV_DENOMINATOR_SIZE_B);
	
	return data;
}//endfunction: read_reg_MXAXB_denominator



/*!   function used to read the Mxaxb divider \n
arguments: none \n
returns: double precision floating point value, the Mxaxb divider value \n
documentation: RM page 103 \n
*/
double si5345::read_MXAXB()
{
	uint64_t numerator = read_reg_MXAXB_numerator();
	uint64_t denominator = read_reg_MXAXB_denominator();
	double fraction_value = 1.0 * numerator / denominator;
	
	return fraction_value;
}//endfunction: read_MXAXB



/*!   function used to update the Mxaxb divider with the currently configured numerator/denominator values \n
arguments: none \n
returns: none \n
documentation: RM page 103 \n
*/
void si5345::update_MXAXB()
{
	preamble_reg_modifications();
	mInterface->write_register(REG_MXAXB_UPDATE, 1);
	postamble_reg_modifications();
}//endfunction: update_MXAXB





//Pxaxb functions

/*! function used to set the Pxaxb divider \n
arguments: value={0,1,2,3} for prescale values: 1,2,4,8 \n
documentation: reference manual page 101 \n
*/
void si5345::set_pxapxb(uint8_t value)
{
	assert(value < REG_PXAXB_MAX);
	preamble_reg_modifications();
	mInterface->write_register(REG_PXAXB, value);
	postamble_reg_modifications();
}//endfunction: set_pxapxb



/*!  function used to read the Pxaxb divider \n
arguments: none \n
return: read value=[0:3] for prescale values: 1,2,4,8 \n
documentation: reference manual page 101 \n
*/
uint8_t si5345::read_reg_pxapxb()
{
	uint8_t data;
	data = mInterface->read_register(REG_PXAXB);
	data = data & REG_FIELD_PXAXB_MASK;
	
	return data;
}//endfunction: read_reg_pxapxb



/*!  function used to read the Pxaxb divider value used \n
arguments: none \n
return: prescaler value = 1,2,4,8 \n
documentation: reference manual page 101 \n
*/
uint8_t si5345::read_divider_pxaxb()
{
	return 1 << read_reg_pxapxb();
}//endfunction: read_divider_pxaxb





//  P functions

/*! function used to actually enable the previously selected numerator and denominator (P) for \n
    a particular input channel \n
arguments: input_channel=[0:3] \n
documentation: reference manual page 102 \n
*/
void si5345::update_P_values(uint8_t input_channel)
{
	uint8_t data = 0;
	
	assert(input_channel < NR_INPUT_CHANNELS);
	
	data = 1 << input_channel;
	mInterface->write_register(REG_Px_UPDATE, data);
}//endfunction: update_P_values



/*!   function used to set the P divider fraction/integer mode \n
arguments: input_channel=[0:3], command: 0 => integer; 1 => fractional mode \n
returns: none \n
documentation: RM page 102 \n
*/
void si5345::set_P_fractional_mode(uint8_t input_channel, uint8_t command)
{
	assert(input_channel<NR_INPUT_CHANNELS);
	assert(command < 2);
	preamble_reg_modifications();
	uint16_t addr = REG_P_FRACTIONAL_MODE_BASE + (input_channel * REG_P_FRACTIONAL_MODE_OFFSET);
	if(command == 0)
	{
		//mInterface->write_register(addr, 0x2B);
	}
	else
	{
		mInterface->write_register(addr, 0x3B);
	}
	postamble_reg_modifications();
}//endfunction: set_M_fractional_mode



/*!  function used to set the numerator on an input channels divider (P) \n
    does not update the value. only sets \n
arguments: input_channel=[0:3]; numerator=48bits \n
documentation: reference manual page 101 \n
*/
void si5345::set_divider_P_numerator(uint8_t input_channel, uint64_t numerator)
{
	uint16_t reg_addr;
	
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(numerator < ((uint64_t) 1 << REG_PX_DIV_NUMERATOR_SIZE_b));
	
	reg_addr = REG_PX_DIV_NUMERATOR_BASE + REG_PX_DIV_NUMERATOR_OFFSET * input_channel;
	
	write_register_by_size(reg_addr, REG_PX_DIV_NUMERATOR_SIZE_B, numerator);
}//endfunction: set_divider_P_numerator



/*!  function used to read the numerator on an input channels divider (P) \n
return: read numerator=48bits \n
arguments: input_channel=[0:3]; \n
documentation: reference manual page 101 \n
*/
uint64_t si5345::read_reg_divider_P_numerator(uint8_t input_channel)
{
	uint16_t reg_addr;
	
	assert(input_channel < NR_INPUT_CHANNELS);
	
	reg_addr = REG_PX_DIV_NUMERATOR_BASE + REG_PX_DIV_NUMERATOR_OFFSET * input_channel;
	
	return read_register_by_size(reg_addr, REG_PX_DIV_NUMERATOR_SIZE_B);
}//endfunction: read_reg_divider_P_numerator



/*!  function used to set the denominator on an input channels divider (P) \n
    does not update the value. only sets \n
arguments: input_channel=[0:3]; denominator=32bits \n
documentation: reference manual page 101 \n
*/
void si5345::set_divider_P_denominator(uint8_t input_channel, uint32_t denominator)
{
	uint16_t reg_addr;
	
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(denominator < ((uint64_t) 1 << REG_PX_DIV_DENOMINATOR_SIZE_b));
	
	reg_addr = REG_PX_DIV_DENOMINATOR_BASE + REG_PX_DIV_DENOMINATOR_OFFSET * input_channel;
	
	write_register_by_size(reg_addr, REG_PX_DIV_DENOMINATOR_SIZE_B, denominator);
}//endfunction: set_divider_P_denominator



/*!  function used to read the denominator of an input channels divider (P) \n
return: read denominator=32b \n
arguments: input_channel=[0:3]; \n
documentation: reference manual page 101 \n
*/
uint32_t si5345::read_reg_divider_P_denominator(uint8_t input_channel)
{
	uint16_t reg_addr;
	
	assert(input_channel < NR_INPUT_CHANNELS);
	
	reg_addr = REG_PX_DIV_DENOMINATOR_BASE + REG_PX_DIV_DENOMINATOR_OFFSET * input_channel;
	return read_register_by_size(reg_addr, REG_PX_DIV_DENOMINATOR_SIZE_B);
}//endfunction: read_reg_divider_P_denominator



/*! function used to set and update the P divider for a particular channel \n
    min value is 1; also updates the value \n
arguments: input_channel=[0:3]; denominator=32b; numerator=48b; \n
documentation: reference manual page 101 \n
*/
void si5345::set_divider_P(uint8_t input_channel, uint64_t numerator, uint32_t denominator)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(numerator >=  denominator);
	assert(denominator < ((uint64_t) 1 << REG_PX_DIV_DENOMINATOR_SIZE_b));
	assert(numerator < ((uint64_t) 1 << REG_PX_DIV_NUMERATOR_SIZE_b));
	
	set_divider_P_numerator(input_channel, numerator);
	set_divider_P_denominator(input_channel, denominator);
	update_P_values(input_channel);
}//endfunction: set_divider_P



/*!   function used to set the P divider \n
computes numerator + denominator and also updates registers \n
arguments: input_channel=[0:3]; double value : divider value \n
returns: none \n
documentation: RM page 101 \n
*/
void si5345::set_divider_P(uint8_t input_channel, double value)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	pair <uint64_t, uint64_t> P_div =  create_fraction(value, REG_PX_DIV_NUMERATOR_SIZE_b, REG_PX_DIV_DENOMINATOR_SIZE_b);
	
	set_divider_P(input_channel,P_div.first, P_div.second);
}//endfunction: set_divider_P



/*! function used to read the P divider for a particular channel \n
return read value; \n
arguments: input_channel=[0:3]; \n
documentation: reference manual page 101 \n
*/
double si5345::read_divider_P(uint8_t input_channel)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	uint64_t numerator = read_reg_divider_P_numerator(input_channel);
	uint64_t denominator = read_reg_divider_P_denominator(input_channel);
	return 1.0 * numerator / denominator;
}//endfunction: read_divider_P





//M functions

/*!   function used to set the M divider numerator \n
does not update value \n
arguments: uint64_t numerator on 56b \n
returns: none \n
documentation: RM page 112 \n
*/
void si5345::set_M_numerator(uint64_t numerator)
{
	assert(numerator < ((uint64_t) 1 << REG_M_DIV_NUMERATOR_SIZE_b));
	
	preamble_reg_modifications();
	write_register_by_size(REG_M_DIV_NUMERATOR_BASE, REG_M_DIV_NUMERATOR_SIZE_B, numerator);
	postamble_reg_modifications();
}//endfunction: set_M_numerator



/*!   function used to set the M divider numerator \n
does not update value \n
arguments: uint64_t denominator on 32b \n
returns: none \n
documentation: RM page 112 \n
*/
void si5345::set_M_denominator(uint32_t denominator)
{
	assert(denominator < ((uint64_t) 1 << REG_M_DIV_DENOMINATOR_SIZE_b));
	
	preamble_reg_modifications();
	write_register_by_size(REG_M_DIV_DENOMINATOR_BASE, REG_M_DIV_DENOMINATOR_SIZE_B, denominator);
	postamble_reg_modifications();
}//endfunction: set_M_denominator



/*!   function used to update the M divider with the currently configured numerator/denominator values \n
arguments: none \n
returns: none \n
documentation: RM page 112 \n
*/
void si5345::update_M()
{
	preamble_reg_modifications();
	mInterface->write_register(REG_M_UPDATE, 1);
	postamble_reg_modifications();
}//endfunction: update_M



/*!   function used to set the Mxaxb fraction/integer mode \n
arguments: command: 0 => integer; 1 => fractional mode \n
returns: none \n
documentation: RM page 112 \n
*/
void si5345::set_M_fractional_mode(uint8_t command)
{
	assert(command < 2);
	preamble_reg_modifications();
	if(command == 0)
	{
		mInterface->write_register(REG_M_FRACTIONAL_MODE, 0x2B);
	}
	else
	{
		mInterface->write_register(REG_M_FRACTIONAL_MODE, 0x3B);
	}
	postamble_reg_modifications();
}//endfunction: set_M_fractional_mode



/*!   function used to set the M divider \n
also updates registers \n
arguments: uint64_t numerator on 56b ,  uint64_t denominator on 32b \n
returns: none \n
documentation: RM page 112 \n
*/
void si5345::set_M(uint64_t numerator,uint32_t denominator)
{
	set_M_numerator(numerator);
	set_M_denominator(denominator);
	update_M();
}//endfunction: set_M



/*!   function used to set the M divider \n
computes numerator + denominator and also updates registers \n
sets fractional mode accordingly \n
arguments: double value : divider value \n
returns: none \n
documentation: RM page 112 \n
*/
void si5345::set_M(double value)
{
	if(value == (uint64_t) value )
	{
		set_M_fractional_mode(0);//integer mode
	}
	else
	{
		set_M_fractional_mode(1);//fractional mode
	}
	
	pair <uint64_t, uint64_t> M_div =  create_fraction(value, REG_M_DIV_NUMERATOR_SIZE_b, REG_M_DIV_DENOMINATOR_SIZE_b);
	
	set_M(M_div.first, M_div.second);
}//endfunction: set_M



/*!   function used to read the M divider numerator \n
arguments: none \n
returns: 56b M numerator value \n
documentation: RM page 112 \n
*/
uint64_t si5345::read_reg_M_numerator()
{
	uint64_t data = read_register_by_size(REG_M_DIV_NUMERATOR_BASE,REG_M_DIV_NUMERATOR_SIZE_B);
	
	return data;
}//endfunction: read_reg_M_numerator



/*!   function used to read the M divider denominator \n
arguments: none \n
returns: 32b M denominator value \n
documentation: RM page 112 \n
*/
uint32_t si5345::read_reg_M_denominator()
{
	uint64_t data = read_register_by_size(REG_M_DIV_DENOMINATOR_BASE,REG_M_DIV_DENOMINATOR_SIZE_B);
	
	return data;
}//endfunction: read_reg_M_denominator



/*!   function used to read the M divider \n
arguments: none \n
returns: double precision floating point value, the M divider value \n
documentation: RM page 112 \n
*/
double si5345::read_divider_M()
{
	uint64_t numerator = read_reg_M_numerator();
	uint64_t denominator = read_reg_M_denominator();
	double fraction_value = 1.0*numerator / denominator;
	
	return fraction_value;
}//endfunction: read_divider_M





//N functions

/*!  function used to actually enable the previously selected numerator and denominator for multisynth dividers N \n
arguments: multisynth=[0:4]; update_all=[0:1]=no/yes; \n
documentation: reference manual page 108 \n
*/
void si5345::update_N_values(uint8_t update_all, uint8_t multisynth)
{
	uint16_t reg_addr;
	uint8_t data;
	
	assert(multisynth < NR_MULTISYNTHS);
	assert(update_all < 2);
	
	if (update_all == 1)//special case. update_all bit is bitfield 1 of N4_divider_update reg
	{
		data = 2;
		reg_addr = 0x338;
	}
	else
	{
		data = 1;
		reg_addr = REG_NX_DIV_UPDATE_BASE + REG_NX_DIV_UPDATE_OFFSET * multisynth;
	}
	
	mInterface->write_register(reg_addr, data);
}//endfunction: update_N_values



/*!  function used to set the numerator on an multisynth divider (N) \n
    does not update the value. only sets \n
arguments: multisynth=[0:4]; numerator=44bits \n
documentation: reference manual page 108 \n
*/
void si5345::set_divider_N_numerator(uint8_t multisynth, uint64_t numerator)
{
	uint16_t reg_addr;
	
	assert(multisynth < NR_MULTISYNTHS);
	assert(numerator < (1LL << REG_NX_DIV_NUMERATOR_SIZE_b));
	
	reg_addr = REG_NX_DIV_NUMERATOR_BASE + REG_NX_DIV_NUMERATOR_OFFSET * multisynth;
	
	write_register_by_size(reg_addr, REG_NX_DIV_NUMERATOR_SIZE_B, numerator);
}//endfunction: set_divider_N_numerator



/*!  function used to read the numerator on an multisynth divider (N) \n
return: the value read, 44b \n
arguments: multisynth=[0:4]; numerator=44bits \n
documentation: reference manual page 108 \n
*/
uint64_t si5345::read_reg_divider_N_numerator(uint8_t multisynth)
{
	uint16_t reg_addr;
	
	assert(multisynth < NR_MULTISYNTHS);
	
	reg_addr = REG_NX_DIV_NUMERATOR_BASE + REG_NX_DIV_NUMERATOR_OFFSET * multisynth;
	uint64_t return_value = read_register_by_size(reg_addr, REG_NX_DIV_NUMERATOR_SIZE_B);
	return return_value;
}//endfunction: read_reg_divider_N_numerator



/*!  function used to set the denominator on a multisynth divider (N) \n
    does not update the value. only sets \n
arguments: input_channel=[0:3]; denominator=32bits \n
documentation: reference manual page 108 \n
*/
void si5345::set_divider_N_denominator(uint8_t multisynth, uint32_t denominator)
{
	uint16_t reg_addr;
	
	assert(multisynth < NR_MULTISYNTHS);
	assert(denominator < ((uint64_t) 1 << REG_NX_DIV_DENOMINATOR_SIZE_b));
	
	reg_addr = REG_NX_DIV_DENOMINATOR_BASE + REG_NX_DIV_DENOMINATOR_OFFSET * multisynth;
	
	write_register_by_size(reg_addr, REG_NX_DIV_DENOMINATOR_SIZE_B, denominator);
}//endfunction: set_divider_N_denominator



/*!  function used to read the denominator on a multisynth divider (N) \n
return: the value read, 32b \n
arguments: input_channel=[0:3]; denominator=32bits \n
documentation: reference manual page 108 \n
*/
uint32_t si5345::read_reg_divider_N_denominator(uint8_t multisynth)
{
	uint16_t reg_addr;
	
	assert(multisynth < NR_MULTISYNTHS);
	
	reg_addr = REG_NX_DIV_DENOMINATOR_BASE + REG_NX_DIV_DENOMINATOR_OFFSET * multisynth;
	uint32_t return_value = read_register_by_size(reg_addr, REG_PX_DIV_DENOMINATOR_SIZE_B);
	return return_value;
}//endfunction: read_reg_divider_N_denominator



/*! function used to set and update the N divider for a particular multisynth \n
    min value is 1; \n
arguments: multisynth=[0:4]; denominator=32b; numerator=48b; \n
documentation: reference manual page 108 \n
*/
void si5345::set_multisynth_divider(uint8_t multisynth, uint64_t numerator, uint32_t denominator)
{
	assert(multisynth < NR_MULTISYNTHS);
	assert(numerator < ((uint64_t) 1 << REG_NX_DIV_NUMERATOR_SIZE_b));
	assert(denominator < ((uint64_t) 1 << REG_NX_DIV_DENOMINATOR_SIZE_b));
	assert(numerator >= denominator);//minimum divider value => denominator > numerator
	
	set_divider_N_numerator(multisynth, numerator);
	set_divider_N_denominator(multisynth, denominator);
	update_N_values(0, multisynth);
}//endfunction: set_multisynth_divider



/*! function used to set and update the N divider for a particular multisynth \n
    min value is 1; \n
arguments: multisynth=[0:4]; double divider_N=divider_value; \n
documentation: reference manual page 108 \n
*/
void si5345::set_multisynth_divider(uint8_t multisynth, double divider_N)
{
	set_N_fractional_mode(multisynth, 0);//set N divider fractional
	pair<uint64_t, uint64_t> fraction = create_fraction(divider_N, REG_NX_DIV_NUMERATOR_SIZE_b, REG_NX_DIV_DENOMINATOR_SIZE_b);
	
	uint64_t numerator_N = fraction.first;
	uint64_t denominator_N = fraction.second;
	
	set_multisynth_divider(multisynth, numerator_N, denominator_N);
}//endfunction: set_multisynth_divider



/*! function used to read the N divider for a particular multisynth \n
returns divider value (the value that the frequency is divided by) \n
arguments: multisynth=[0:4]; \n
documentation: reference manual page 108 \n
*/
double si5345::read_divider_N(uint8_t multisynth)
{
	assert(multisynth < NR_MULTISYNTHS);
	
	return 1.0 * read_reg_divider_N_numerator(multisynth) / read_reg_divider_N_denominator(multisynth);
}//endfunction: read_divider_N



/*!   function used to set the N fraction/integer mode for one N divider \n
arguments: multisynth=selected_multisynth number; command: 0 => fractional; 1 => integer mode \n
returns: none \n
documentation: RM page 121 \n
*/
void si5345::set_N_fractional_mode(uint8_t multisynth, uint8_t command)
{
	assert(command < 2);
	assert(multisynth < NR_MULTISYNTHS);
	uint8_t data = command << multisynth;
	if (command == 0)
	{
		mInterface->write_register(REG_N_FRACTIONAL_MODE, data);
	}
	else
	{
		mInterface->write_register(REG_N_FRACTIONAL_MODE, data);
	}
	
	reset_soft();
}//endfunction: set_N_fractional_mode



/*!   function used to read the N fraction/integer mode for one N divider \n
arguments: multisynth=selected_multisynth number; \n
returns: 0 => fractional; 1 => integer mode \n
documentation: RM page 121 \n
*/
uint8_t si5345::read_N_fractional_mode(uint8_t multisynth)
{
	assert(multisynth < NR_MULTISYNTHS);
	
	return (mInterface->read_register(REG_N_FRACTIONAL_MODE) >> multisynth) & 1;
}//endfunction: read_N_fractional_mode



//R functions
/*! function used to set and update the set_OUT0_RDIV_FORCE2 register (bitfield) \n
arguments: output_channel=[0:9]; value=0/1 divider value is: 0=>set by R0_REG; 1=>forced into divide by 2; \n
documentation: reference manual page 97 \n
*/
void si5345::set_OUT0_RDIV_FORCE2(uint8_t output_channel, uint8_t value)
{
	
	uint16_t reg_addr;
	uint8_t data;
	
	assert(output_channel < NR_OUTPUT_CHANNELS);
	assert(value < 2);
	
	if (output_channel == 9) // special case
	{
		reg_addr = 0x013A;
	}
	else
	{
		reg_addr = REG_CLK_OUT_DRIVER_CONIFG_BASE + REG_CLK_OUT_DRIVER_CONIFG_OFFSET * output_channel;
	}
	
	data = mInterface->read_register(reg_addr);
	data = (data & ~REG_FIELD_OUTX_RDIV_FORCE2_MASK) | (value << REG_FIELD_OUTX_RDIV_FORCE2_POSITION);
	mInterface->write_register(reg_addr, data);
}//endfunction: set_OUT0_RDIV_FORCE2



/*! function used to set and update the set_OUT0_RDIV_FORCE2 register (bitfield) \n
return: OUT0_RDIV_FORCE2 value: 0/1: 0=>divider value is set by R0_REG; 1=>forced into divide by 2; \n
arguments: output_channel=[0:9]; \n
documentation: reference manual page 97 \n
*/
uint8_t si5345::read_reg_OUT0_RDIV_FORCE2(uint8_t output_channel)
{
	uint16_t reg_addr;
	uint8_t data;
	
	assert(output_channel < NR_OUTPUT_CHANNELS);
	
	if (output_channel == 9) // special case
	{
		reg_addr = 0x013A;
	}
	else
	{
		reg_addr = REG_CLK_OUT_DRIVER_CONIFG_BASE + REG_CLK_OUT_DRIVER_CONIFG_OFFSET * output_channel;
	}
	
	data = mInterface->read_register(reg_addr);
	data = (data & REG_FIELD_OUTX_RDIV_FORCE2_MASK) >> REG_FIELD_OUTX_RDIV_FORCE2_POSITION;
	
	return data;
}//endfunction: read_reg_OUT0_RDIV_FORCE2



/*! function used to set and update the R divider for a particular output channel \n
    only supports integer division; divide value (denominator) = (R0_REG+1) x 2; \n
    R0_REG calculation done internatlly, can set a slightly different freq than desired, based on eq ^. \n
arguments: output_channel=[0:9]; denominator=24b; \n
documentation: reference manual page 104 \n
*/
void si5345::set_output_divider(uint8_t output_channel, uint32_t denominator)
{
	uint16_t reg_addr;
	
	assert(output_channel < 10);
	assert(denominator < (uint64_t) 1 << REG_NX_DIV_DENOMINATOR_SIZE_b);
	assert(denominator >= 2);
	assert(denominator % 2 == 0);
	
	if (denominator == 2)
	{
		set_OUT0_RDIV_FORCE2(output_channel, 1);
	}
	else
	{
		set_OUT0_RDIV_FORCE2(output_channel, 0);
		if (output_channel == 9) // special case
		{
			reg_addr = 0x0268;
		}
		else
		{
			reg_addr = REG_RX_REG_BASE + REG_RX_REG_OFFSET * output_channel;
		}
		uint32_t RX_reg_data = (denominator / 2 - 1);
		write_register_by_size(reg_addr, REG_RX_DIV_DENOMINATOR_SIZE_B, RX_reg_data);
	}
	
}//endfunction: set_output_divider



/*!  function used to read the R divider for a particular output channel \n
    only returns the value of the reg, not the force by 2 bit from config \n
return: 24b denominator read \n
arguments: output_channel=[0:9]; \n
documentation: reference manual page 104 \n
*/
uint32_t si5345::read_divider_RX_REG(uint8_t output_channel)
{
	uint16_t reg_addr;
	
	assert(output_channel < NR_OUTPUT_CHANNELS);
	
	if (output_channel == 9) // special cases
	{
		reg_addr = 0x0268;
	}
	else
	{
		reg_addr = REG_RX_REG_BASE + REG_RX_REG_OFFSET * output_channel;
	}
	
	return read_register_by_size(reg_addr, REG_RX_DIV_DENOMINATOR_SIZE_B);
}//endfunction: read_divider_RX_REG



/*!  function used to read the R divider denominator used for a particular output channel \n
return: 24b RX denominator used by device \n
arguments: output_channel=[0:9]; \n
documentation : none  \n
*/
uint32_t si5345::read_divider_RX_denominator(uint8_t output_channel)
{
	assert(output_channel < NR_OUTPUT_CHANNELS);
	
	if (read_reg_OUT0_RDIV_FORCE2(output_channel) == 1)
	{
		return 2;
	}
	else
	{
		return (read_divider_RX_REG(output_channel) + 1) * 2;
	}
	
}//endfunction: read_divider_RX_denominator



/*!  function used to read the R divider used for a particular output channel \n
    as R divider only has a denominator, it returns that value; \n
return: 24b RX divider used by device \n
arguments: output_channel=[0:9]; \n
documentation : none  \n
*/
double si5345::read_divider_R(uint8_t output_channel)
{
	assert(output_channel < NR_OUTPUT_CHANNELS);
	
	return 1.0 * read_divider_RX_denominator(output_channel);
}//endfunction: read_divider_R



/*!  function used to enter freerun mode \n
configures the Pxaxb + Mxaxb registers \n
arguments: none \n
returns: none \n
documentation : reference manual pag 9 + 100 + 102 \n
*/
void si5345::enable_freerun_mode()
{
	uint64_t pxaxb_xtal_frequency = 0;
	uint8_t pxaxb = 0;
	
	pxaxb_xtal_frequency = mXtalFreq / 1.0;/// (1 << pxaxb);
	
	set_pxapxb(pxaxb);
	double mxaxb = 1.0 * mDSPLLOutFrequency / pxaxb_xtal_frequency;
	set_MXAXB(mxaxb);
}//endfunction: enable_freerun_mode



/*!   function used to read the input clock select register: bits: IN_ACTIV [7:6] \n
arguments : none  \n
returns value of IN_ACTIV bitfield: can be [0:3] \n
documentation: reference manual page 111 \n
*/
uint8_t si5345::read_reg_input_clock_select_in_sel()
{
	uint8_t data;
	data = mInterface->read_register(REG_INPUT_CLOCK_SELECT_ACTIVE);
	data = (data & REG_FIELD_IN_SEL_ACTIVE_MASK) >> REG_FIELD_IN_SEL_ACTIVE_POSITION;
	
	return data;
}//endfunction: read_reg_input_clock_select_in_sel



/*!   function used to write the input clock select register: bits: IN_ACTIV [7:6] \n
argument: input to be selected \n
returns: none \n
documentation: reference manual page 111 \n
*/
void si5345::write_reg_input_clock_select_in_sel(uint8_t input_channel)
{
	uint8_t data;
	data = (input_channel << REG_FIELD_IN_SEL_POSITION) & REG_FIELD_IN_SEL_MASK;
	data = data | 0x01; // bit0==1 => register controlled clock selection
	mInterface->write_register(REG_INPUT_CLOCK_SELECT, data);
}//endfunction: write_reg_input_clock_select_in_sel



/*!  function used to find an unused multysinth \n
return: index of unused multysinth, or -1 if all multysinths are in use \n
arguments:  vector<int> out_ch_en vector of enables. value of 1 means the particular output channel is enabled; \n
        map<int8_t, int8_t> output_ch_multisynth_connections multysinth to output_channel connections matrix; \n
documentation : none  \n
*/
int8_t si5345::find_empty_multysinth(vector<int8_t> out_ch_en, map<int8_t, int8_t> output_ch_multisynth_connections) {
	vector<int8_t> free_multysinths;
	
	for (int8_t i = 0; i < NR_MULTISYNTHS; i++)
	{
		free_multysinths.push_back(0);
	}
	
	for (int8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)//find empty multysinth
	{
		if (out_ch_en[i] == 1)
		{
			free_multysinths[output_ch_multisynth_connections[i]] = 1;
		}
	}
	
	for (int8_t i = 0; i < NR_MULTISYNTHS; i++)
	{
		if (free_multysinths[i] == 0)
		{
			return i;
		}
	}
	
	return -1;
}//endfunction: find_empty_multysinth



/*!  function used to find a multysinth with only one output channel attached \n
return: index of single used multysinth, or -1 if none are found \n
arguments:  vector<int> out_ch_en vector of enables. value of 1 means the particular output channel is enabled; \n
        map<int8_t, int8_t> output_ch_multisynth_connections  output_channel to multysinth connections matrix; \n
documentation : none  \n
*/
int8_t si5345::find_single_connected_multysinth(vector<int8_t> out_ch_en, map<int8_t, int8_t> output_ch_multisynth_connections)
{
	vector<int8_t> free_multysinths;
	
	for (int8_t i = 0; i < NR_MULTISYNTHS; i++)
	{
		free_multysinths.push_back(0);
	}
	
	for (int8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)//find empty multysinth
	{
		free_multysinths[output_ch_multisynth_connections[i]] += out_ch_en[i];
	}
	
	for (int8_t i = 0; i < NR_MULTISYNTHS; i++)
	{
		if (free_multysinths[i] == 1)
		{
			return i;
		}
	}
	
	return -1;
}//endfunction: find_single_connected_multysinth



/*!  function used to find a multysinth with only one output channel attached \n
return: a vector of output channels connected to the particular multisynth \n
arguments:  multysinth = the multysinth for which the connected output channels are to be returned \n
        vector<int> out_ch_en vector of enables. value of 1 means the particular output channel is enabled; \n
        map<int8_t, int8_t> output_ch_multisynth_connections  output_channel to multysinth connections matrix; \n
documentation : none  \n
*/
vector<int8_t> si5345::find_output_channels_connected_to_multysinth(int8_t multysinth, vector<int8_t> out_ch_en,
																	map<int8_t, int8_t> output_ch_multisynth_connections)
{
	vector<int8_t> output_channels_connected_to_multysinth;
	for (int8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)//find empty multysinth
	{
		if ((out_ch_en[i] == 1) && (output_ch_multisynth_connections[i] == multysinth))
		{
			output_channels_connected_to_multysinth.push_back(i);
		}
	}
	
	return output_channels_connected_to_multysinth;
}//endfunction: find_output_channels_connected_to_multysinth



/*! function used to calculate the greatest common divider of 2 numbers \n
returns: greatest common divider of the 2 numbers; \n
arguments: 2 uint64_t numbers; \n
docuemntation : none  \n
*/
uint64_t si5345::gcd(uint64_t a, uint64_t b)
{
	if ((a == 1) && (b == 1))
	{
		return 1;
	}
	if (b == 0)
	{
		return a;
	}
	return gcd(b, a % b);
}//endfunction: gcd



/*! function used to calculate the lowest common multiple of 2 numbers \n
returns: lowest common multiple of the 2 numbers; \n
arguments: 2 uint64_t numbers; \n
documentation: none  \n
*/
uint64_t si5345::lcm(uint64_t a, uint64_t b)
{
	return (a * b) / gcd(a, b);
}//endfunction: lcm



/*! function used to create a fraction (denominator and numerator for fraction target_value \n
also left shifts as much as possible \n
returns: pair<uint64_t,uint64_t> = numerator,denominator; \n
arguments:  double target_value = desired fraction value; \n
        uint8_t max_bits_denominator = maximum number of bits for output denominator; \n
        uint8_t max_bits_numerator = maximum number of bits for output numerator; \n
documentation : none  \n
*/
pair<uint64_t, uint64_t> si5345::create_fraction(double target_value, uint8_t max_bits_numerator, uint8_t max_bits_denominator)
{
	pair<uint64_t, uint64_t> result;
	
	uint64_t numerator = 0;
	uint64_t denominator = 1;
	double target_value_c = target_value;
	
	while (target_value_c != (uint64_t) target_value_c)//while has digits after decimal point
	{
		target_value_c *= 10;
		denominator *= 10;
	}
	numerator = target_value_c;
	
	uint64_t gcd0 = gcd(numerator, denominator);
	
	numerator /= gcd0;
	denominator /= gcd0;
	
	while (
		(numerator > ((uint64_t) 1 << (max_bits_numerator - 1)))        ||
		(denominator > ((uint64_t) 1 << (max_bits_denominator - 1)))
		)
	{
		numerator >>= 1;
		denominator >>= 1;
	}
	
	assert(numerator < ((uint64_t) 1 << max_bits_numerator));
	assert(denominator < ((uint64_t) 1 << max_bits_denominator));
	
	//left shift as much as possible
	while (
		(numerator < ((uint64_t) 1 << (max_bits_numerator - 1)))         &&
		(denominator < ((uint64_t) 1 << (max_bits_denominator - 1)))
		)
	{
		numerator <<= 1;
		denominator <<= 1;
	}
	
	return make_pair(numerator, denominator);
}//endfunction: create_fraction



/*!   function used to enable/disable all outputs simultaneously \n
uses OUTALL_DISABLE_LOW register \n
arguments: 1 Pass through the output enables, 0 disables all output drivers \n
returns : none  \n
documentation: reference manual page 97 \n
*/
void si5345::control_output_en_all(uint8_t command)
{
	assert(command < 2);
	mInterface->write_register(REG_OUTALL_DISABLE_LOW, command);
}//endfunction: control_output_en_all



/*!   function used to read the OUTALL_DISABLE_LOW register \n
arguments : none  \n
return read value \n
documentation: reference manual page 97 \n
*/
uint8_t si5345::read_reg_output_en_all()
{
	return mInterface->read_register(REG_OUTALL_DISABLE_LOW);
}//endfunction: read_reg_output_en_all

/*!   function used to read if a particular output channel is enabled or not \n
does not take global output enable/disable into account \n
returns data read: 1/0 \n
arguments: output_channel: [0:9] \n
documentation: reference manual page 97 \n
*/
uint8_t si5345::read_reg_output_en(uint8_t output_channel)
{
	uint16_t reg_addr;    // base addr - output ch0//0x01-08
	uint8_t data;
	
	assert(output_channel < NR_OUTPUT_CHANNELS);
	
	if (output_channel == 9) //special case
	{
		reg_addr = 0x013A;
	}
	else
	{
		reg_addr = REG_CLK_OUT_DRIVER_CONIFG_BASE + REG_CLK_OUT_DRIVER_CONIFG_OFFSET * output_channel;
	}
	
	data = mInterface->read_register(reg_addr);
	data = (data & REG_FIELD_OUTX_OE_MASK) >> REG_FIELD_OUTX_OE_POSITION;
	
	return data;
}//endfunction: read_reg_output_en



/*!   function used to read if a particular output channel is enabled or not \n
also takes global output enable/disable into account \n
returns 0 if output channel is disabled, 1 if output channel is enabled \n
arguments: output_channel: [0:9] that is checked \n
documentation: reference manual page 97 \n
*/
int si5345::check_enable_output_channel(uint8_t output_channel)
{
	assert(output_channel < NR_OUTPUT_CHANNELS);
	return read_reg_output_en(output_channel) & read_reg_output_en_all();
}//endfunction: check_enable_output_channel



/*!   function used to connect an output channel to a particular multisynth \n
arguments: multisynth (source)=[0:4];   output_channel=[0:9] \n
returns: none \n
documentation: reference manual page 98 \n
*/
void si5345::select_multisynth_for_output_channel(uint8_t multisynth, uint8_t output_channel)
{
	uint16_t reg_addr;
	int data;
	
	assert(output_channel < NR_OUTPUT_CHANNELS);
	assert(multisynth < NR_MULTISYNTHS);
	
	if (output_channel == 9) //special case
	{
		reg_addr = 0x013D;
	}
	else
	{
		reg_addr = REG_R_DIVX_MUX_SEL_BASE + REG_R_DIVX_MUX_SEL_OFFSET * output_channel;
	}
	
	data = mInterface->read_register(reg_addr);
	data = (data & ~REG_FIELD_OUTX_MUX_SEL_MASK) | (multisynth << REG_FIELD_OUTX_MUX_SEL_POSITION);
	data = data | 0x08 ; //XXXX aici am adaugat pt test;	OUT0_VDD_SEL_EN
	mInterface->write_register(reg_addr, data);
}//endfunction: select_multisynth_for_output_channel



/*!   function used to read what multisynth a particular output channel is connected to \n
returns value read \n
arguments: output_channel=[0:9] \n
documentation: reference manual page 98 \n
*/
uint8_t si5345::read_multisynth_for_output_channel(uint8_t output_channel)
{
	uint16_t reg_addr;
	int data;
	
	assert(output_channel < NR_OUTPUT_CHANNELS);
	
	if (output_channel == 9) //special case
	{
		reg_addr = 0x013D;
	}
	else
	{
		reg_addr = REG_R_DIVX_MUX_SEL_BASE + REG_R_DIVX_MUX_SEL_OFFSET * output_channel;
	}
	
	data = mInterface->read_register(reg_addr);
	data = (data & REG_FIELD_OUTX_MUX_SEL_MASK) >> REG_FIELD_OUTX_MUX_SEL_POSITION;
	
	return data;
}//endfunction: read_multisynth_for_output_channel



/*!   function used to read all multisynth-output channel connections \n
arguments : none  \n
returns map<int8_t,int8_t>: key = output channel; data = multisynth (source) \n
documentation: reference manual page 98 \n
*/
map<int8_t, int8_t> si5345::read_connections_output_channels_multisynths()
{
	map<int8_t, int8_t> connections;
	
	for (int8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)
	{
		connections[i] = read_multisynth_for_output_channel(i);
	}
	
	return connections;
}//endfunction: read_connections_output_channels_multisynths



/*!  function used to read the frequency used on an output channel \n
does not take disable/enable into account \n
returns: output frequency in hertz \n
arguments: desired_frequency=in hertz, output_channel=[0:9]; \n
documentation : none  \n
*/
uint64_t si5345::read_freq_on_output_channel_no_disable(uint8_t output_channel)
{
	uint8_t connected_multisynth;
	connected_multisynth = read_multisynth_for_output_channel(output_channel);
	return (uint64_t) (1.0 * mDSPLLOutFrequency / read_divider_N(connected_multisynth) / read_divider_R(output_channel));
}//endfunction: read_freq_on_output_channel_no_disable



/*!   function used to read and print the device id, grade and revision \n
arguments : none \n
returns : the chip id as a char array \n
documentation : RM page 82 \n
*/
std::string si5345::read_id()
{
	char *chip_id = new char[16];
	uint16_t pn_base;
	uint8_t grade;
	uint8_t device_rev;
	
	pn_base = read_register_by_size(0x02, 2);
	grade = mInterface->read_register(0x04);
	device_rev = mInterface->read_register(0x05);
	
	sprintf(chip_id, "%x-%c-%c", pn_base, grade + 'A', device_rev + 'A');
	
	return chip_id;
}//endfunction: read_id



/*!   function used to hard reset the device \n
same as powerup \n
arguments : none \n
returns : none  \n
documentation: reference manual page 87 \n
*/
void si5345::reset_hard()
{
	mInterface->write_register(REG_HARD_RST, 1 << REG_HARD_RST_POSITION);
	usleep(300 * 1000);//1s
}//endfunction: reset_hard



/*!   function used to Initialize and calibrate the entire device \n
arguments : none \n
returns : none  \n
documentation: reference manual page 86 \n
*/
void si5345::reset_soft_all()
{
	mInterface->write_register(REG_SOFT_RST_ALL, 1 << REG_SOFT_RST_ALL_POSITION);
	usleep(1000 * 1000);//1s
}//endfunction: reset_soft_all



/*!   function used to Initialize outer loop \n
arguments : none \n
returns : none  \n
documentation: reference manual page 86 \n
*/
void si5345::reset_soft()
{
	mInterface->write_register(REG_SOFT_RST, 1 << REG_SOFT_RST_POSITION);
	usleep(1000 * 1000);//TO DO: fix it
}//endfunction: reset_soft



/*! function used to select the input format for an input channel \n
arguments:  \n
    channel: selected channel \n
    format =    0: differential \n
                1: cmos \n
returns: none \n
documentation: RM page 22 \n
*/
void si5345::set_input_format(uint8_t input_channel, uint8_t format)
{
	assert(input_channel < NR_INPUT_CHANNELS);
	assert(format <= 1);
	
	uint8_t data = 0;
	uint8_t in_en = mInterface->read_register(REG_IN_EN) & REG_IN_EN_MASK;
	
	data = ( format << (input_channel + REG_FIELD_IN_PULSED_CMOS_EN_POSITION) ) | in_en;
	
	uint16_t reg_addr = REG_IN_PULSED_CMOS_EN;
	
	mInterface->write_register(reg_addr, data);
}//endfunction: set_output_format




/*! function used to select the output format for an output channel \n
arguments: \n
output_channel = selected channel \n
format =    1: swing differential, normal swing \n
            2: swing differential, high swing \n
            4: LVCMOS single ended \n
            5: LVCMOS +pin only \n
            6: LVCMOS -pin only \n
out_sync = power down + out enable synced w/ out clk; 0=disable, 1=enable \n
disable_state = output driver state when disabled; 0=low, 1=high \n
cmos_drv = lvcmos output impedance; XXXX=CMOS1; XXXX=CMOS2; XXXX=CMOS3 \n
returns: none \n
documentation: RM page 97 \n
*/
void si5345::set_output_format(uint8_t output_channel, uint8_t format, uint8_t out_sync, uint8_t disable_state, uint8_t cmos_drv)
{
	assert(format < 7);
	assert(format != 0);
	assert(format != 3);
	
	assert(output_channel < NR_OUTPUT_CHANNELS);
	assert(out_sync < 3);
	assert(disable_state < 3);
	//assert(cmos_drv < 3); cred ??? si totusi intrun plan de CB era cred 1/2/3 fara 0
	
	uint8_t data = 0;
	data = data | (cmos_drv << REG_FIELD_OUT_FORMAT_CMOS_DRV_POSITION);
	data = data | (disable_state << REG_FIELD_OUT_FORMAT_DISABLE_STATE_POSITION);
	data = data | (out_sync << REG_FIELD_OUT_FORMAT_OUT_SYNC_EN_POSITION);
	data = data | (format << REG_FIELD_OUT_FORMAT_FORMAT_POSITION);
	
	uint16_t reg_addr;
	
	if (output_channel == 9) //special case
	{
		reg_addr = 0x013B;
	}
	else
	{
		reg_addr = REG_OUT_DRIVER_FORMAT_BASE + REG_OUT_DRIVER_FORMAT_OFFSET * output_channel;
	}
	
	mInterface->write_register(reg_addr, data);
}//endfunction: set_output_format



/*!   function used to enable or disable a particular output channel \n
arguments: command: 0=disable; 1=enable; in total 10 output channels \n
returns : none  \n
documentation: reference manual page 97 \n
*/
void si5345::enable_output(uint8_t output_channel, uint8_t command)
{
	uint16_t reg_addr;    // base addr - output ch0//0x01-08
	uint8_t data;
	
	assert(output_channel < NR_OUTPUT_CHANNELS);
	assert(command < 2);
	
	if (output_channel == 9) //special case
	{
		reg_addr = 0x013A;
	}
	else
	{
		reg_addr = REG_CLK_OUT_DRIVER_CONIFG_BASE + REG_CLK_OUT_DRIVER_CONIFG_OFFSET * output_channel;
	}
	
	if (command == 1)
	{
		control_output_en_all(1);
	}
	
	data = mInterface->read_register(reg_addr);
	data = data & ~REG_FIELD_OUTX_OE_MASK;
	data |= ((command << REG_FIELD_OUTX_OE_POSITION) & REG_FIELD_OUTX_OE_MASK);
	
	data = data & ~REG_FIELD_OUTX_PDN_MASK;
	data |= ((0 << REG_FIELD_OUTX_PDN_POSITION) & REG_FIELD_OUTX_PDN_MASK);
	
	mInterface->write_register(reg_addr, data);
}//endfunction: enable_output



/*!  function used to calculate and set a desired frequency for an output channel \n
    also enables that particular channel \n
    warning: if all multysinths are busy driving an output channel, one of the output channels \n
    in use will be disabled while connecting the desired out_ch to the same multysinth. \n
arguments: desired_frequency=in hertz, output_channel=[0:9]; \n
returns : none  \n
documentation : none  \n
*/
void si5345::set_frequency(uint64_t desired_frequency, uint8_t output_channel)
{
	vector<int8_t> out_ch_en;
	int8_t nr_enabled_chs = 0;
	
	enable_output(output_channel, 0);
	
	for (int8_t i = 0; i < NR_OUTPUT_CHANNELS; i++)
	{
		out_ch_en.push_back(read_reg_output_en(i));
		nr_enabled_chs += out_ch_en[i];
	}
	
	map<int8_t, int8_t> output_ch_multisynth_connections = read_connections_output_channels_multisynths();
	
	if (nr_enabled_chs < 5)
	{
		int8_t empty_multysinth_index = find_empty_multysinth(out_ch_en, output_ch_multisynth_connections);
		select_multisynth_for_output_channel(empty_multysinth_index, output_channel);
		
		//mDSPLLOutFrequency / divider_n / denominator_R = desired_frequency; main equation
		uint64_t numerator_N;
		uint32_t denominator_N;
		uint32_t denominator_R = 2;
		double divider_N = 1.0 * mDSPLLOutFrequency / (desired_frequency * denominator_R);
		set_N_fractional_mode(empty_multysinth_index, 0);//set N divider fractional
		
		double N_out_freq = mDSPLLOutFrequency / divider_N;
		if (N_out_freq < MULTISYNTH_OUT_MIN_FREQ)
		{
			uint64_t factor = (MULTISYNTH_OUT_MIN_FREQ / N_out_freq) + 1;
			divider_N = divider_N / factor;
			denominator_R = denominator_R * factor;
		}
		
		pair<uint64_t, uint64_t> fraction = create_fraction(divider_N, REG_NX_DIV_NUMERATOR_SIZE_b, REG_NX_DIV_DENOMINATOR_SIZE_b);
		
		numerator_N = fraction.first;
		denominator_N = fraction.second;
		
		set_output_divider(output_channel, denominator_R);
		set_multisynth_divider(empty_multysinth_index, numerator_N, denominator_N);
		
		enable_output(output_channel, 1);
		
	}
	else// 5 < nr_enabled_chs < 10
	{
		int8_t single_connected_multisynth = find_single_connected_multysinth(out_ch_en, output_ch_multisynth_connections);
		int8_t out_ch_to_disable = find_output_channels_connected_to_multysinth(single_connected_multisynth,
																				out_ch_en, output_ch_multisynth_connections)[0];
		
		uint64_t desired_freq_out_ch_a = desired_frequency;
		uint64_t current_freq_out_ch_b = read_freq_on_out_ch(out_ch_to_disable);
		
		enable_output(out_ch_to_disable, 0);
		set_N_fractional_mode(single_connected_multisynth, 0);//set N divider fractional
		select_multisynth_for_output_channel(single_connected_multisynth, output_channel);
		
		//Main equations:
		//mDSPLLOutFrequency / divider_N / denominator_R1 = out1_freq; //desired_frequency;
		//mDSPLLOutFrequency / divider_N / denominator_R2 = out2_freq; //already set
		
		uint64_t numerator_N;
		uint32_t denominator_N;
		
		uint64_t lcm_a_b = lcm(desired_freq_out_ch_a, current_freq_out_ch_b);
		
		if((2.0 * lcm_a_b) > mDSPLLOutFrequency )
		{
			printf("LCM error. selected frequency cannot be obtained. known bug. read README");
		}
		else
		{
			double divider_N = mDSPLLOutFrequency / (2.0 * lcm_a_b);
			//2* in order to divde by 2*R because R_reg must be even value
			
			uint64_t dividerR1 = lcm_a_b / desired_freq_out_ch_a;
			uint64_t dividerR2 = lcm_a_b / current_freq_out_ch_b;
			
			double N_out_freq = mDSPLLOutFrequency / divider_N;
			uint64_t factor;
			if (N_out_freq < MULTISYNTH_OUT_MIN_FREQ)
			{
				factor = (MULTISYNTH_OUT_MIN_FREQ / N_out_freq) + 1;
				divider_N = divider_N / factor;
				dividerR1 = dividerR1 * factor;
				dividerR2 = dividerR2 * factor;
			}
			
			dividerR1 = dividerR1 * 2;//bigger divider here because lcm is *2 and is used to compute the 2 dividers
			dividerR2 = dividerR2 * 2;
			
			if ((dividerR1 % 2 == 1) || (dividerR2 % 2 == 1))
			{
				dividerR1 = dividerR1 * 2;
				dividerR2 = dividerR2 * 2;
				divider_N = divider_N / 2;
			}
			
			pair<uint64_t, uint64_t> fraction = create_fraction(divider_N, REG_NX_DIV_NUMERATOR_SIZE_b, REG_NX_DIV_DENOMINATOR_SIZE_b);
			numerator_N = fraction.first;
			denominator_N = fraction.second;
			
			set_output_divider(output_channel, dividerR1);
			set_output_divider(out_ch_to_disable, dividerR2);
			set_multisynth_divider(single_connected_multisynth, numerator_N, denominator_N);
			
			enable_output(output_channel, 1);
		}
		enable_output(out_ch_to_disable, 1);
	}
	control_output_en_all(1);
}//endfunction: set_frequency



/*!  function used to read the frequency used on an output channel \n
takes disable/enable into account \n
returns: output frequency in hertz; if disabled returns 0; \n
arguments: desired_frequency=in hertz, output_channel=[0:9]; \n
docuementation : none  \n
*/
uint64_t si5345::read_freq_on_out_ch(uint8_t output_channel)
{
	if (read_reg_output_en(output_channel) == 1)
	{
		return read_freq_on_output_channel_no_disable(output_channel);
	}
	else
	{
		return 0;
	}
}//endfunction: read_freq_on_out_ch



/*!  function used to read multiple bytes from a register \n
reads higher address reg on msB of result. lower address on lsB of result \n
returns read value \n  \n
arguments: reg_addr=address from which to read; size=nr of bytes to read \n
documentation: none  \n
*/
uint64_t si5345::read_register_by_size(uint16_t reg_addr, uint8_t size)
{
	uint64_t data = 0;
	
	for (uint16_t i = reg_addr + size - 1; i >= reg_addr; i--)
	{
		//uint8_t temp = mInterface->read_register(i);
		data = (data << 8) | mInterface->read_register(i);
	}
	
	return data;
}//endfunction: read_register_by_size



/*!  function used to write a single multiple bytes to a register \n
writes msB on higher address. lsB on lower address \n
arguments: reg_addr=address to which the writing is done; size=nr of bytes to write \n
    value=what to write to the register \n
documentation : none  \n
*/
void si5345::write_register_by_size(uint16_t reg_addr, uint8_t size, uint64_t value)
{
	
	uint64_t data = value;
	
	for (uint16_t i = reg_addr; i < reg_addr + size; i++)
	{
		mInterface->write_register(i, data);
		data >>= 8;
	}
}//endfunction: write_register_by_size



/*!  function used to set all registers in order to prepare for freerun mode freq generation \n
values obtained from clock builder pro. divider registers cleared. \n
arguments: none \n
returns : none  \n
documentation : clock builder pro generated \n
*/
void si5345::set_registers_freerun()
{
	si5345_revd_register_t const regs[526] =
		{
			/* Start configuration preamble */
			{ 0x0B24, 0xC0 },
			{ 0x0B25, 0x00 },
			{ 0x0540, 0x01 },
			/* End configuration preamble */
			
			/* Delay 300 msec */
			/*    Delay is worst case time for device to complete any calibration */
			/*    that is running due to device state change previous to this script */
			/*    being processed. */
			
			/* Start configuration registers */
			{ 0x0006, 0x00 },
			{ 0x0007, 0x00 },
			{ 0x0008, 0x00 },
			{ 0x000B, 0x68 },
			{ 0x0016, 0x02 },
			{ 0x0017, 0xDC },
			{ 0x0018, 0xFF },
			{ 0x0019, 0xFF },
			{ 0x001A, 0xFF },
			{ 0x002B, 0x02 },
			{ 0x002C, 0x00 },
			{ 0x002D, 0x00 },
			{ 0x002E, 0x00 },
			{ 0x002F, 0x00 },
			{ 0x0030, 0x00 },
			{ 0x0031, 0x00 },
			{ 0x0032, 0x00 },
			{ 0x0033, 0x00 },
			{ 0x0034, 0x00 },
			{ 0x0035, 0x00 },
			{ 0x0036, 0x00 },
			{ 0x0037, 0x00 },
			{ 0x0038, 0x00 },
			{ 0x0039, 0x00 },
			{ 0x003A, 0x00 },
			{ 0x003B, 0x00 },
			{ 0x003C, 0x00 },
			{ 0x003D, 0x00 },
			{ 0x003F, 0x00 },
			{ 0x0040, 0x04 },
			{ 0x0041, 0x00 },
			{ 0x0042, 0x00 },
			{ 0x0043, 0x00 },
			{ 0x0044, 0x00 },
			{ 0x0045, 0x0C },
			{ 0x0046, 0x00 },
			{ 0x0047, 0x00 },
			{ 0x0048, 0x00 },
			{ 0x0049, 0x00 },
			{ 0x004A, 0x00 },
			{ 0x004B, 0x00 },
			{ 0x004C, 0x00 },
			{ 0x004D, 0x00 },
			{ 0x004E, 0x00 },
			{ 0x004F, 0x00 },
			{ 0x0050, 0x0F },
			{ 0x0051, 0x00 },
			{ 0x0052, 0x00 },
			{ 0x0053, 0x00 },
			{ 0x0054, 0x00 },
			{ 0x0055, 0x00 },
			{ 0x0056, 0x00 },
			{ 0x0057, 0x00 },
			{ 0x0058, 0x00 },
			{ 0x0059, 0x00 },
			{ 0x005A, 0x00 },
			{ 0x005B, 0x00 },
			{ 0x005C, 0x00 },
			{ 0x005D, 0x00 },
			{ 0x005E, 0x00 },
			{ 0x005F, 0x00 },
			{ 0x0060, 0x00 },
			{ 0x0061, 0x00 },
			{ 0x0062, 0x00 },
			{ 0x0063, 0x00 },
			{ 0x0064, 0x00 },
			{ 0x0065, 0x00 },
			{ 0x0066, 0x00 },
			{ 0x0067, 0x00 },
			{ 0x0068, 0x00 },
			{ 0x0069, 0x00 },
			{ 0x0092, 0x00 },
			{ 0x0093, 0x00 },
			{ 0x0095, 0x00 },
			{ 0x0096, 0x00 },
			{ 0x0098, 0x00 },
			{ 0x009A, 0x00 },
			{ 0x009B, 0x00 },
			{ 0x009D, 0x00 },
			{ 0x009E, 0x00 },
			{ 0x00A0, 0x00 },
			{ 0x00A2, 0x00 },
			{ 0x00A9, 0x00 },
			{ 0x00AA, 0x00 },
			{ 0x00AB, 0x00 },
			{ 0x00AC, 0x00 },
			{ 0x00E5, 0x01 },
			{ 0x00EA, 0x00 },
			{ 0x00EB, 0x00 },
			{ 0x00EC, 0x00 },
			{ 0x00ED, 0x00 },
			{ 0x0102, 0x00 },
			{ 0x0108, 0x04 },
			{ 0x0109, 0xCC },
			{ 0x010A, 0x00 },
			{ 0x010B, 0x18 },
			{ 0x010D, 0x01 },
			{ 0x010E, 0x09 },
			{ 0x010F, 0x3B },
			{ 0x0110, 0x28 },
			{ 0x0112, 0x01 },
			{ 0x0113, 0x09 },
			{ 0x0114, 0x3B },
			{ 0x0115, 0x28 },
			{ 0x0117, 0x01 },
			{ 0x0118, 0x09 },
			{ 0x0119, 0x3B },
			{ 0x011A, 0x28 },
			{ 0x011C, 0x01 },
			{ 0x011D, 0x09 },
			{ 0x011E, 0x3B },
			{ 0x011F, 0x28 },
			{ 0x0121, 0x01 },
			{ 0x0122, 0x09 },
			{ 0x0123, 0x3B },
			{ 0x0124, 0x28 },
			{ 0x0126, 0x01 },
			{ 0x0127, 0x09 },
			{ 0x0128, 0x3B },
			{ 0x0129, 0x28 },
			{ 0x012B, 0x01 },
			{ 0x012C, 0x09 },
			{ 0x012D, 0x3B },
			{ 0x012E, 0x28 },
			{ 0x0130, 0x01 },
			{ 0x0131, 0x09 },
			{ 0x0132, 0x3B },
			{ 0x0133, 0x28 },
			{ 0x013A, 0x01 },
			{ 0x013B, 0x09 },
			{ 0x013C, 0x3B },
			{ 0x013D, 0x28 },
			{ 0x013F, 0x00 },
			{ 0x0140, 0x00 },
			{ 0x0141, 0x40 },
			{ 0x0142, 0xFF },
			{ 0x0206, 0x00 },
			{ 0x0208, 0x00 },
			{ 0x0209, 0x00 },
			{ 0x020A, 0x00 },
			{ 0x020B, 0x00 },
			{ 0x020C, 0x00 },
			{ 0x020D, 0x00 },
			{ 0x020E, 0x00 },
			{ 0x020F, 0x00 },
			{ 0x0210, 0x00 },
			{ 0x0211, 0x00 },
			{ 0x0212, 0x00 },
			{ 0x0213, 0x00 },
			{ 0x0214, 0x00 },
			{ 0x0215, 0x00 },
			{ 0x0216, 0x00 },
			{ 0x0217, 0x00 },
			{ 0x0218, 0x00 },
			{ 0x0219, 0x00 },
			{ 0x021A, 0x00 },
			{ 0x021B, 0x00 },
			{ 0x021C, 0x00 },
			{ 0x021D, 0x00 },
			{ 0x021E, 0x00 },
			{ 0x021F, 0x00 },
			{ 0x0220, 0x00 },
			{ 0x0221, 0x00 },
			{ 0x0222, 0x00 },
			{ 0x0223, 0x00 },
			{ 0x0224, 0x00 },
			{ 0x0225, 0x00 },
			{ 0x0226, 0x00 },
			{ 0x0227, 0x00 },
			{ 0x0228, 0x00 },
			{ 0x0229, 0x00 },
			{ 0x022A, 0x00 },
			{ 0x022B, 0x00 },
			{ 0x022C, 0x00 },
			{ 0x022D, 0x00 },
			{ 0x022E, 0x00 },
			{ 0x022F, 0x00 },
			{ 0x0231, 0x0B },
			{ 0x0232, 0x0B },
			{ 0x0233, 0x0B },
			{ 0x0234, 0x0B },
			{ 0x0235, 0x00 },
			{ 0x0236, 0x00 },
			{ 0x0237, 0x00 },
			{ 0x0238, 0x80 },
			{ 0x0239, 0x89 },
			{ 0x023A, 0x00 },
			{ 0x023B, 0x00 },
			{ 0x023C, 0x00 },
			{ 0x023D, 0x00 },
			{ 0x023E, 0x80 },
			{ 0x024A, 0x00 },
			{ 0x024B, 0x00 },
			{ 0x024C, 0x00 },
			{ 0x024D, 0x00 },
			{ 0x024E, 0x00 },
			{ 0x024F, 0x00 },
			{ 0x0250, 0x00 },
			{ 0x0251, 0x00 },
			{ 0x0252, 0x00 },
			{ 0x0253, 0x00 },
			{ 0x0254, 0x00 },
			{ 0x0255, 0x00 },
			{ 0x0256, 0x00 },
			{ 0x0257, 0x00 },
			{ 0x0258, 0x00 },
			{ 0x0259, 0x00 },
			{ 0x025A, 0x00 },
			{ 0x025B, 0x00 },
			{ 0x025C, 0x00 },
			{ 0x025D, 0x00 },
			{ 0x025E, 0x00 },
			{ 0x025F, 0x00 },
			{ 0x0260, 0x00 },
			{ 0x0261, 0x00 },
			{ 0x0262, 0x00 },
			{ 0x0263, 0x00 },
			{ 0x0264, 0x00 },
			{ 0x0268, 0x00 },
			{ 0x0269, 0x00 },
			{ 0x026A, 0x00 },
			{ 0x026B, 0x35 },
			{ 0x026C, 0x33 },
			{ 0x026D, 0x34 },
			{ 0x026E, 0x35 },
			{ 0x026F, 0x45 },
			{ 0x0270, 0x56 },
			{ 0x0271, 0x42 },
			{ 0x0272, 0x32 },
			{ 0x028A, 0x00 },
			{ 0x028B, 0x00 },
			{ 0x028C, 0x00 },
			{ 0x028D, 0x00 },
			{ 0x028E, 0x00 },
			{ 0x028F, 0x00 },
			{ 0x0290, 0x00 },
			{ 0x0291, 0x00 },
			{ 0x0294, 0x80 },
			{ 0x0296, 0x00 },
			{ 0x0297, 0x00 },
			{ 0x0299, 0x00 },
			{ 0x029D, 0x00 },
			{ 0x029E, 0x00 },
			{ 0x029F, 0x00 },
			{ 0x02A9, 0x00 },
			{ 0x02AA, 0x00 },
			{ 0x02AB, 0x00 },
			{ 0x02B7, 0xFF },
			{ 0x0302, 0x00 },
			{ 0x0303, 0x00 },
			{ 0x0304, 0x00 },
			{ 0x0305, 0x00 },
			{ 0x0306, 0x00 },
			{ 0x0307, 0x00 },
			{ 0x0308, 0x00 },
			{ 0x0309, 0x00 },
			{ 0x030A, 0x00 },
			{ 0x030B, 0x00 },
			{ 0x030C, 0x00 },
			{ 0x030D, 0x00 },
			{ 0x030E, 0x00 },
			{ 0x030F, 0x00 },
			{ 0x0310, 0x00 },
			{ 0x0311, 0x00 },
			{ 0x0312, 0x00 },
			{ 0x0313, 0x00 },
			{ 0x0314, 0x00 },
			{ 0x0315, 0x00 },
			{ 0x0316, 0x00 },
			{ 0x0317, 0x00 },
			{ 0x0318, 0x00 },
			{ 0x0319, 0x00 },
			{ 0x031A, 0x00 },
			{ 0x031B, 0x00 },
			{ 0x031C, 0x00 },
			{ 0x031D, 0x00 },
			{ 0x031E, 0x00 },
			{ 0x031F, 0x00 },
			{ 0x0320, 0x00 },
			{ 0x0321, 0x00 },
			{ 0x0322, 0x00 },
			{ 0x0323, 0x00 },
			{ 0x0324, 0x00 },
			{ 0x0325, 0x00 },
			{ 0x0326, 0x00 },
			{ 0x0327, 0x00 },
			{ 0x0328, 0x00 },
			{ 0x0329, 0x00 },
			{ 0x032A, 0x00 },
			{ 0x032B, 0x00 },
			{ 0x032C, 0x00 },
			{ 0x032D, 0x00 },
			{ 0x032E, 0x00 },
			{ 0x032F, 0x00 },
			{ 0x0330, 0x00 },
			{ 0x0331, 0x00 },
			{ 0x0332, 0x00 },
			{ 0x0333, 0x00 },
			{ 0x0334, 0x00 },
			{ 0x0335, 0x00 },
			{ 0x0336, 0x00 },
			{ 0x0337, 0x00 },
			{ 0x0338, 0x00 },
			{ 0x0339, 0x1F },
			{ 0x033B, 0x00 },
			{ 0x033C, 0x00 },
			{ 0x033D, 0x00 },
			{ 0x033E, 0x00 },
			{ 0x033F, 0x00 },
			{ 0x0340, 0x00 },
			{ 0x0341, 0x00 },
			{ 0x0342, 0x00 },
			{ 0x0343, 0x00 },
			{ 0x0344, 0x00 },
			{ 0x0345, 0x00 },
			{ 0x0346, 0x00 },
			{ 0x0347, 0x00 },
			{ 0x0348, 0x00 },
			{ 0x0349, 0x00 },
			{ 0x034A, 0x00 },
			{ 0x034B, 0x00 },
			{ 0x034C, 0x00 },
			{ 0x034D, 0x00 },
			{ 0x034E, 0x00 },
			{ 0x034F, 0x00 },
			{ 0x0350, 0x00 },
			{ 0x0351, 0x00 },
			{ 0x0352, 0x00 },
			{ 0x0353, 0x00 },
			{ 0x0354, 0x00 },
			{ 0x0355, 0x00 },
			{ 0x0356, 0x00 },
			{ 0x0357, 0x00 },
			{ 0x0358, 0x00 },
			{ 0x0359, 0x00 },
			{ 0x035A, 0x00 },
			{ 0x035B, 0x00 },
			{ 0x035C, 0x00 },
			{ 0x035D, 0x00 },
			{ 0x035E, 0x00 },
			{ 0x035F, 0x00 },
			{ 0x0360, 0x00 },
			{ 0x0361, 0x00 },
			{ 0x0362, 0x00 },
			{ 0x0487, 0x00 },
			{ 0x0508, 0x00 },
			{ 0x0509, 0x00 },
			{ 0x050A, 0x00 },
			{ 0x050B, 0x00 },
			{ 0x050C, 0x00 },
			{ 0x050D, 0x00 },
			{ 0x050E, 0x00 },
			{ 0x050F, 0x00 },
			{ 0x0510, 0x00 },
			{ 0x0511, 0x00 },
			{ 0x0512, 0x00 },
			{ 0x0513, 0x00 },
			{ 0x0515, 0x00 },
			{ 0x0516, 0x00 },
			{ 0x0517, 0x00 },
			{ 0x0518, 0x00 },
			{ 0x0519, 0x00 },
			{ 0x051A, 0x00 },
			{ 0x051B, 0x00 },
			{ 0x051C, 0x00 },
			{ 0x051D, 0x00 },
			{ 0x051E, 0x00 },
			{ 0x051F, 0x00 },
			{ 0x0521, 0x2B },
			{ 0x052A, 0x00 },
			{ 0x052B, 0x01 },
			{ 0x052C, 0x0F },
			{ 0x052D, 0x03 },
			{ 0x052E, 0x00 },
			{ 0x052F, 0x00 },
			{ 0x0531, 0x00 },
			{ 0x0532, 0x00 },
			{ 0x0533, 0x04 },
			{ 0x0534, 0x00 },
			{ 0x0535, 0x01 },
			{ 0x0536, 0x06 },
			{ 0x0537, 0x00 },
			{ 0x0538, 0x00 },
			{ 0x0539, 0x00 },
			{ 0x053D, 0x0A },
			{ 0x053E, 0x06 },
			{ 0x0589, 0x0C },
			{ 0x058A, 0x00 },
			{ 0x059B, 0x18 },
			{ 0x059D, 0x00 },
			{ 0x059E, 0x00 },
			{ 0x059F, 0x00 },
			{ 0x05A0, 0x00 },
			{ 0x05A1, 0x00 },
			{ 0x05A2, 0x00 },
			{ 0x05A6, 0x00 },
			{ 0x0802, 0x35 },
			{ 0x0803, 0x05 },
			{ 0x0804, 0x01 },
			{ 0x0805, 0x00 },
			{ 0x0806, 0x00 },
			{ 0x0807, 0x00 },
			{ 0x0808, 0x00 },
			{ 0x0809, 0x00 },
			{ 0x080A, 0x00 },
			{ 0x080B, 0x00 },
			{ 0x080C, 0x00 },
			{ 0x080D, 0x00 },
			{ 0x080E, 0x00 },
			{ 0x080F, 0x00 },
			{ 0x0810, 0x00 },
			{ 0x0811, 0x00 },
			{ 0x0812, 0x00 },
			{ 0x0813, 0x00 },
			{ 0x0814, 0x00 },
			{ 0x0815, 0x00 },
			{ 0x0816, 0x00 },
			{ 0x0817, 0x00 },
			{ 0x0818, 0x00 },
			{ 0x0819, 0x00 },
			{ 0x081A, 0x00 },
			{ 0x081B, 0x00 },
			{ 0x081C, 0x00 },
			{ 0x081D, 0x00 },
			{ 0x081E, 0x00 },
			{ 0x081F, 0x00 },
			{ 0x0820, 0x00 },
			{ 0x0821, 0x00 },
			{ 0x0822, 0x00 },
			{ 0x0823, 0x00 },
			{ 0x0824, 0x00 },
			{ 0x0825, 0x00 },
			{ 0x0826, 0x00 },
			{ 0x0827, 0x00 },
			{ 0x0828, 0x00 },
			{ 0x0829, 0x00 },
			{ 0x082A, 0x00 },
			{ 0x082B, 0x00 },
			{ 0x082C, 0x00 },
			{ 0x082D, 0x00 },
			{ 0x082E, 0x00 },
			{ 0x082F, 0x00 },
			{ 0x0830, 0x00 },
			{ 0x0831, 0x00 },
			{ 0x0832, 0x00 },
			{ 0x0833, 0x00 },
			{ 0x0834, 0x00 },
			{ 0x0835, 0x00 },
			{ 0x0836, 0x00 },
			{ 0x0837, 0x00 },
			{ 0x0838, 0x00 },
			{ 0x0839, 0x00 },
			{ 0x083A, 0x00 },
			{ 0x083B, 0x00 },
			{ 0x083C, 0x00 },
			{ 0x083D, 0x00 },
			{ 0x083E, 0x00 },
			{ 0x083F, 0x00 },
			{ 0x0840, 0x00 },
			{ 0x0841, 0x00 },
			{ 0x0842, 0x00 },
			{ 0x0843, 0x00 },
			{ 0x0844, 0x00 },
			{ 0x0845, 0x00 },
			{ 0x0846, 0x00 },
			{ 0x0847, 0x00 },
			{ 0x0848, 0x00 },
			{ 0x0849, 0x00 },
			{ 0x084A, 0x00 },
			{ 0x084B, 0x00 },
			{ 0x084C, 0x00 },
			{ 0x084D, 0x00 },
			{ 0x084E, 0x00 },
			{ 0x084F, 0x00 },
			{ 0x0850, 0x00 },
			{ 0x0851, 0x00 },
			{ 0x0852, 0x00 },
			{ 0x0853, 0x00 },
			{ 0x0854, 0x00 },
			{ 0x0855, 0x00 },
			{ 0x0856, 0x00 },
			{ 0x0857, 0x00 },
			{ 0x0858, 0x00 },
			{ 0x0859, 0x00 },
			{ 0x085A, 0x00 },
			{ 0x085B, 0x00 },
			{ 0x085C, 0x00 },
			{ 0x085D, 0x00 },
			{ 0x085E, 0x00 },
			{ 0x085F, 0x00 },
			{ 0x0860, 0x00 },
			{ 0x0861, 0x00 },
			{ 0x090E, 0x02 },
			{ 0x0943, 0x01 },
			{ 0x0949, 0x00 },
			{ 0x094A, 0x00 },
			{ 0x094E, 0x49 },
			{ 0x094F, 0x02 },
			{ 0x095E, 0x00 },
			{ 0x0A02, 0x00 },
			{ 0x0A03, 0x1F },
			{ 0x0A04, 0x01 },
			{ 0x0A05, 0x1F },
			{ 0x0A14, 0x00 },
			{ 0x0A1A, 0x00 },
			{ 0x0A20, 0x00 },
			{ 0x0A26, 0x00 },
			{ 0x0A2C, 0x00 },
			{ 0x0B44, 0x0F },
			{ 0x0B46, 0x00 },
			{ 0x0B47, 0x0F },
			{ 0x0B48, 0x0F },
			{ 0x0B4A, 0x00 },
			{ 0x0B57, 0x0E },
			{ 0x0B58, 0x01 },
			/* End configuration registers */
			
			/* Start configuration postamble */
			{ 0x0514, 0x01 },
			{ 0x001C, 0x01 },
			{ 0x0540, 0x00 },
			{ 0x0B24, 0xC3 },
			{ 0x0B25, 0x02 },
			/* End configuration postamble */
		};
	
	write_register_by_size(regs[0].address,1,regs[0].value);
	write_register_by_size(regs[1].address,1,regs[1].value);
	write_register_by_size(regs[2].address,1,regs[2].value);
	usleep(400 * 1000);
	for(int i = 3; i< 526; i++)
	{
		write_register_by_size(regs[i].address,1,regs[i].value);
	}
	usleep(400 * 1000);
}//endfunction: set_registers freerun



/*!  function used to set all registers in order to prepare for input controlled freq generation \n
values obtained from clock builder pro. divider registers cleared. \n
arguments: none \n
returns : none  \n
documentation : clock builder pro generated  \n
*/
void si5345::set_registers_input_controlled()	//in 0,1,2,3  -  1M
{
	si5345_revd_register_t const regs[526] =
		{
			/* Start configuration preamble */
			{ 0x0B24, 0xC0 },
			{ 0x0B25, 0x00 },
			{ 0x0540, 0x01 },
			/* End configuration preamble */
			
			/* Delay 300 msec */
			/*    Delay is worst case time for device to complete any calibration */
			/*    that is running due to device state change previous to this script */
			/*    being processed. */
			
			/* Start configuration registers */
			{ 0x0006, 0x00 },
			{ 0x0007, 0x00 },
			{ 0x0008, 0x00 },
			{ 0x000B, 0x68 },
			{ 0x0016, 0x02 },
			{ 0x0017, 0xDC },
			{ 0x0018, 0x00 },
			{ 0x0019, 0xDD },
			{ 0x001A, 0xDF },
			{ 0x002B, 0x02 },
			{ 0x002C, 0x0F },
			{ 0x002D, 0x55 },
			{ 0x002E, 0x3F },
			{ 0x002F, 0x01 },
			{ 0x0030, 0x3F },
			{ 0x0031, 0x01 },
			{ 0x0032, 0x3F },
			{ 0x0033, 0x01 },
			{ 0x0034, 0x3F },
			{ 0x0035, 0x01 },
			{ 0x0036, 0x3F },
			{ 0x0037, 0x01 },
			{ 0x0038, 0x3F },
			{ 0x0039, 0x01 },
			{ 0x003A, 0x3F },
			{ 0x003B, 0x01 },
			{ 0x003C, 0x3F },
			{ 0x003D, 0x01 },
			{ 0x003F, 0xFF },
			{ 0x0040, 0x04 },
			{ 0x0041, 0x06 },
			{ 0x0042, 0x06 },
			{ 0x0043, 0x06 },
			{ 0x0044, 0x06 },
			{ 0x0045, 0x0C },
			{ 0x0046, 0x32 },
			{ 0x0047, 0x32 },
			{ 0x0048, 0x32 },
			{ 0x0049, 0x32 },
			{ 0x004A, 0x32 },
			{ 0x004B, 0x32 },
			{ 0x004C, 0x32 },
			{ 0x004D, 0x32 },
			{ 0x004E, 0x55 },
			{ 0x004F, 0x55 },
			{ 0x0050, 0x0F },
			{ 0x0051, 0x03 },
			{ 0x0052, 0x03 },
			{ 0x0053, 0x03 },
			{ 0x0054, 0x03 },
			{ 0x0055, 0x03 },
			{ 0x0056, 0x03 },
			{ 0x0057, 0x03 },
			{ 0x0058, 0x03 },
			{ 0x0059, 0x55 },
			{ 0x005A, 0x55 },
			{ 0x005B, 0x55 },
			{ 0x005C, 0x55 },
			{ 0x005D, 0x01 },
			{ 0x005E, 0x55 },
			{ 0x005F, 0x55 },
			{ 0x0060, 0x55 },
			{ 0x0061, 0x01 },
			{ 0x0062, 0x55 },
			{ 0x0063, 0x55 },
			{ 0x0064, 0x55 },
			{ 0x0065, 0x01 },
			{ 0x0066, 0x55 },
			{ 0x0067, 0x55 },
			{ 0x0068, 0x55 },
			{ 0x0069, 0x01 },
			{ 0x0092, 0x02 },
			{ 0x0093, 0xA0 },
			{ 0x0095, 0x00 },
			{ 0x0096, 0x80 },
			{ 0x0098, 0x60 },
			{ 0x009A, 0x02 },
			{ 0x009B, 0x60 },
			{ 0x009D, 0x04 },
			{ 0x009E, 0x40 },
			{ 0x00A0, 0x20 },
			{ 0x00A2, 0x00 },
			{ 0x00A9, 0xDB },
			{ 0x00AA, 0x31 },
			{ 0x00AB, 0x00 },
			{ 0x00AC, 0x00 },
			{ 0x00E5, 0x00 },
			{ 0x00EA, 0x05 },
			{ 0x00EB, 0x30 },
			{ 0x00EC, 0x00 },
			{ 0x00ED, 0x00 },
			{ 0x0102, 0x01 },
			{ 0x0108, 0x00 },
			{ 0x0109, 0x09 },
			{ 0x010A, 0x3B },
			{ 0x010B, 0x28 },
			{ 0x010D, 0x00 },
			{ 0x010E, 0x09 },
			{ 0x010F, 0x3B },
			{ 0x0110, 0x28 },
			{ 0x0112, 0x00 },
			{ 0x0113, 0x09 },
			{ 0x0114, 0x3B },
			{ 0x0115, 0x28 },
			{ 0x0117, 0x00 },
			{ 0x0118, 0x09 },
			{ 0x0119, 0x3B },
			{ 0x011A, 0x28 },
			{ 0x011C, 0x00 },
			{ 0x011D, 0x09 },
			{ 0x011E, 0x3B },
			{ 0x011F, 0x28 },
			{ 0x0121, 0x00 },
			{ 0x0122, 0x09 },
			{ 0x0123, 0x3B },
			{ 0x0124, 0x28 },
			{ 0x0126, 0x00 },
			{ 0x0127, 0x09 },
			{ 0x0128, 0x3B },
			{ 0x0129, 0x28 },
			{ 0x012B, 0x00 },
			{ 0x012C, 0x09 },
			{ 0x012D, 0x3B },
			{ 0x012E, 0x28 },
			{ 0x0130, 0x00 },
			{ 0x0131, 0x09 },
			{ 0x0132, 0x3B },
			{ 0x0133, 0x28 },
			{ 0x013A, 0x00 },
			{ 0x013B, 0x09 },
			{ 0x013C, 0x3B },
			{ 0x013D, 0x28 },
			{ 0x013F, 0x00 },
			{ 0x0140, 0x00 },
			{ 0x0141, 0x40 },
			{ 0x0142, 0xFF },
			{ 0x0206, 0x00 },
			{ 0x0208, 0x05 },
			{ 0x0209, 0x00 },
			{ 0x020A, 0x00 },
			{ 0x020B, 0x00 },
			{ 0x020C, 0x00 },
			{ 0x020D, 0x00 },
			{ 0x020E, 0x01 },
			{ 0x020F, 0x00 },
			{ 0x0210, 0x00 },
			{ 0x0211, 0x00 },
			{ 0x0212, 0x05 },
			{ 0x0213, 0x00 },
			{ 0x0214, 0x00 },
			{ 0x0215, 0x00 },
			{ 0x0216, 0x00 },
			{ 0x0217, 0x00 },
			{ 0x0218, 0x01 },
			{ 0x0219, 0x00 },
			{ 0x021A, 0x00 },
			{ 0x021B, 0x00 },
			{ 0x021C, 0x05 },
			{ 0x021D, 0x00 },
			{ 0x021E, 0x00 },
			{ 0x021F, 0x00 },
			{ 0x0220, 0x00 },
			{ 0x0221, 0x00 },
			{ 0x0222, 0x01 },
			{ 0x0223, 0x00 },
			{ 0x0224, 0x00 },
			{ 0x0225, 0x00 },
			{ 0x0226, 0x05 },
			{ 0x0227, 0x00 },
			{ 0x0228, 0x00 },
			{ 0x0229, 0x00 },
			{ 0x022A, 0x00 },
			{ 0x022B, 0x00 },
			{ 0x022C, 0x01 },
			{ 0x022D, 0x00 },
			{ 0x022E, 0x00 },
			{ 0x022F, 0x00 },
			{ 0x0231, 0x0B },
			{ 0x0232, 0x0B },
			{ 0x0233, 0x0B },
			{ 0x0234, 0x0B },
			{ 0x0235, 0x00 },
			{ 0x0236, 0x00 },
			{ 0x0237, 0x00 },
			{ 0x0238, 0xA0 },
			{ 0x0239, 0x89 },
			{ 0x023A, 0x00 },
			{ 0x023B, 0x00 },
			{ 0x023C, 0x00 },
			{ 0x023D, 0x00 },
			{ 0x023E, 0x80 },
			{ 0x024A, 0x01 },
			{ 0x024B, 0x00 },
			{ 0x024C, 0x00 },
			{ 0x024D, 0x01 },
			{ 0x024E, 0x00 },
			{ 0x024F, 0x00 },
			{ 0x0250, 0x01 },
			{ 0x0251, 0x00 },
			{ 0x0252, 0x00 },
			{ 0x0253, 0x01 },
			{ 0x0254, 0x00 },
			{ 0x0255, 0x00 },
			{ 0x0256, 0x01 },
			{ 0x0257, 0x00 },
			{ 0x0258, 0x00 },
			{ 0x0259, 0x01 },
			{ 0x025A, 0x00 },
			{ 0x025B, 0x00 },
			{ 0x025C, 0x01 },
			{ 0x025D, 0x00 },
			{ 0x025E, 0x00 },
			{ 0x025F, 0x01 },
			{ 0x0260, 0x00 },
			{ 0x0261, 0x00 },
			{ 0x0262, 0x01 },
			{ 0x0263, 0x00 },
			{ 0x0264, 0x00 },
			{ 0x0268, 0x01 },
			{ 0x0269, 0x00 },
			{ 0x026A, 0x00 },
			{ 0x026B, 0x00 },
			{ 0x026C, 0x00 },
			{ 0x026D, 0x00 },
			{ 0x026E, 0x00 },
			{ 0x026F, 0x00 },
			{ 0x0270, 0x00 },
			{ 0x0271, 0x00 },
			{ 0x0272, 0x00 },
			{ 0x028A, 0x00 },
			{ 0x028B, 0x00 },
			{ 0x028C, 0x00 },
			{ 0x028D, 0x00 },
			{ 0x028E, 0x00 },
			{ 0x028F, 0x00 },
			{ 0x0290, 0x00 },
			{ 0x0291, 0x00 },
			{ 0x0294, 0xC0 },
			{ 0x0296, 0x02 },
			{ 0x0297, 0x02 },
			{ 0x0299, 0x02 },
			{ 0x029D, 0x19 },
			{ 0x029E, 0x01 },
			{ 0x029F, 0x00 },
			{ 0x02A9, 0x66 },
			{ 0x02AA, 0x02 },
			{ 0x02AB, 0x00 },
			{ 0x02B7, 0xFF },
			{ 0x0302, 0x00 },
			{ 0x0303, 0x00 },
			{ 0x0304, 0x00 },
			{ 0x0305, 0x80 },
			{ 0x0306, 0x73 },
			{ 0x0307, 0x06 },
			{ 0x0308, 0x00 },
			{ 0x0309, 0x00 },
			{ 0x030A, 0x00 },
			{ 0x030B, 0x80 },
			{ 0x030C, 0x00 },
			{ 0x030D, 0x00 },
			{ 0x030E, 0x00 },
			{ 0x030F, 0x00 },
			{ 0x0310, 0x00 },
			{ 0x0311, 0x00 },
			{ 0x0312, 0x00 },
			{ 0x0313, 0x00 },
			{ 0x0314, 0x00 },
			{ 0x0315, 0x00 },
			{ 0x0316, 0x00 },
			{ 0x0317, 0x00 },
			{ 0x0318, 0x00 },
			{ 0x0319, 0x00 },
			{ 0x031A, 0x00 },
			{ 0x031B, 0x00 },
			{ 0x031C, 0x00 },
			{ 0x031D, 0x00 },
			{ 0x031E, 0x00 },
			{ 0x031F, 0x00 },
			{ 0x0320, 0x00 },
			{ 0x0321, 0x00 },
			{ 0x0322, 0x00 },
			{ 0x0323, 0x00 },
			{ 0x0324, 0x00 },
			{ 0x0325, 0x00 },
			{ 0x0326, 0x00 },
			{ 0x0327, 0x00 },
			{ 0x0328, 0x00 },
			{ 0x0329, 0x00 },
			{ 0x032A, 0x00 },
			{ 0x032B, 0x00 },
			{ 0x032C, 0x00 },
			{ 0x032D, 0x00 },
			{ 0x032E, 0x00 },
			{ 0x032F, 0x00 },
			{ 0x0330, 0x00 },
			{ 0x0331, 0x00 },
			{ 0x0332, 0x00 },
			{ 0x0333, 0x00 },
			{ 0x0334, 0x00 },
			{ 0x0335, 0x00 },
			{ 0x0336, 0x00 },
			{ 0x0337, 0x00 },
			{ 0x0338, 0x00 },
			{ 0x0339, 0x1F },
			{ 0x033B, 0x00 },
			{ 0x033C, 0x00 },
			{ 0x033D, 0x00 },
			{ 0x033E, 0x00 },
			{ 0x033F, 0x00 },
			{ 0x0340, 0x00 },
			{ 0x0341, 0x00 },
			{ 0x0342, 0x00 },
			{ 0x0343, 0x00 },
			{ 0x0344, 0x00 },
			{ 0x0345, 0x00 },
			{ 0x0346, 0x00 },
			{ 0x0347, 0x00 },
			{ 0x0348, 0x00 },
			{ 0x0349, 0x00 },
			{ 0x034A, 0x00 },
			{ 0x034B, 0x00 },
			{ 0x034C, 0x00 },
			{ 0x034D, 0x00 },
			{ 0x034E, 0x00 },
			{ 0x034F, 0x00 },
			{ 0x0350, 0x00 },
			{ 0x0351, 0x00 },
			{ 0x0352, 0x00 },
			{ 0x0353, 0x00 },
			{ 0x0354, 0x00 },
			{ 0x0355, 0x00 },
			{ 0x0356, 0x00 },
			{ 0x0357, 0x00 },
			{ 0x0358, 0x00 },
			{ 0x0359, 0x00 },
			{ 0x035A, 0x00 },
			{ 0x035B, 0x00 },
			{ 0x035C, 0x00 },
			{ 0x035D, 0x00 },
			{ 0x035E, 0x00 },
			{ 0x035F, 0x00 },
			{ 0x0360, 0x00 },
			{ 0x0361, 0x00 },
			{ 0x0362, 0x00 },
			{ 0x0487, 0x00 },
			{ 0x0508, 0x13 },
			{ 0x0509, 0x22 },
			{ 0x050A, 0x0C },
			{ 0x050B, 0x0B },
			{ 0x050C, 0x07 },
			{ 0x050D, 0x07 },
			{ 0x050E, 0x17 },
			{ 0x050F, 0x2C },
			{ 0x0510, 0x08 },
			{ 0x0511, 0x07 },
			{ 0x0512, 0x07 },
			{ 0x0513, 0x07 },
			{ 0x0515, 0x00 },
			{ 0x0516, 0x00 },
			{ 0x0517, 0x00 },
			{ 0x0518, 0x00 },
			{ 0x0519, 0xCE },
			{ 0x051A, 0x19 },
			{ 0x051B, 0x00 },
			{ 0x051C, 0x00 },
			{ 0x051D, 0x00 },
			{ 0x051E, 0x00 },
			{ 0x051F, 0x80 },
			{ 0x0521, 0x2B },
			{ 0x052A, 0x01 },
			{ 0x052B, 0x01 },
			{ 0x052C, 0x87 },
			{ 0x052D, 0x03 },
			{ 0x052E, 0x19 },
			{ 0x052F, 0x19 },
			{ 0x0531, 0x00 },
			{ 0x0532, 0x8E },
			{ 0x0533, 0x20 },
			{ 0x0534, 0x00 },
			{ 0x0535, 0x00 },
			{ 0x0536, 0x04 },
			{ 0x0537, 0x00 },
			{ 0x0538, 0x00 },
			{ 0x0539, 0x00 },
			{ 0x053A, 0x02 },
			{ 0x053B, 0x03 },
			{ 0x053C, 0x00 },
			{ 0x053D, 0x0E },
			{ 0x053E, 0x06 },
			{ 0x0589, 0x81 },
			{ 0x058A, 0x00 },
			{ 0x059B, 0xF8 },
			{ 0x059D, 0x13 },
			{ 0x059E, 0x24 },
			{ 0x059F, 0x0C },
			{ 0x05A0, 0x0B },
			{ 0x05A1, 0x07 },
			{ 0x05A2, 0x07 },
			{ 0x05A6, 0x03 },
			{ 0x0802, 0x35 },
			{ 0x0803, 0x05 },
			{ 0x0804, 0x00 },
			{ 0x0805, 0x00 },
			{ 0x0806, 0x00 },
			{ 0x0807, 0x00 },
			{ 0x0808, 0x00 },
			{ 0x0809, 0x00 },
			{ 0x080A, 0x00 },
			{ 0x080B, 0x00 },
			{ 0x080C, 0x00 },
			{ 0x080D, 0x00 },
			{ 0x080E, 0x00 },
			{ 0x080F, 0x00 },
			{ 0x0810, 0x00 },
			{ 0x0811, 0x00 },
			{ 0x0812, 0x00 },
			{ 0x0813, 0x00 },
			{ 0x0814, 0x00 },
			{ 0x0815, 0x00 },
			{ 0x0816, 0x00 },
			{ 0x0817, 0x00 },
			{ 0x0818, 0x00 },
			{ 0x0819, 0x00 },
			{ 0x081A, 0x00 },
			{ 0x081B, 0x00 },
			{ 0x081C, 0x00 },
			{ 0x081D, 0x00 },
			{ 0x081E, 0x00 },
			{ 0x081F, 0x00 },
			{ 0x0820, 0x00 },
			{ 0x0821, 0x00 },
			{ 0x0822, 0x00 },
			{ 0x0823, 0x00 },
			{ 0x0824, 0x00 },
			{ 0x0825, 0x00 },
			{ 0x0826, 0x00 },
			{ 0x0827, 0x00 },
			{ 0x0828, 0x00 },
			{ 0x0829, 0x00 },
			{ 0x082A, 0x00 },
			{ 0x082B, 0x00 },
			{ 0x082C, 0x00 },
			{ 0x082D, 0x00 },
			{ 0x082E, 0x00 },
			{ 0x082F, 0x00 },
			{ 0x0830, 0x00 },
			{ 0x0831, 0x00 },
			{ 0x0832, 0x00 },
			{ 0x0833, 0x00 },
			{ 0x0834, 0x00 },
			{ 0x0835, 0x00 },
			{ 0x0836, 0x00 },
			{ 0x0837, 0x00 },
			{ 0x0838, 0x00 },
			{ 0x0839, 0x00 },
			{ 0x083A, 0x00 },
			{ 0x083B, 0x00 },
			{ 0x083C, 0x00 },
			{ 0x083D, 0x00 },
			{ 0x083E, 0x00 },
			{ 0x083F, 0x00 },
			{ 0x0840, 0x00 },
			{ 0x0841, 0x00 },
			{ 0x0842, 0x00 },
			{ 0x0843, 0x00 },
			{ 0x0844, 0x00 },
			{ 0x0845, 0x00 },
			{ 0x0846, 0x00 },
			{ 0x0847, 0x00 },
			{ 0x0848, 0x00 },
			{ 0x0849, 0x00 },
			{ 0x084A, 0x00 },
			{ 0x084B, 0x00 },
			{ 0x084C, 0x00 },
			{ 0x084D, 0x00 },
			{ 0x084E, 0x00 },
			{ 0x084F, 0x00 },
			{ 0x0850, 0x00 },
			{ 0x0851, 0x00 },
			{ 0x0852, 0x00 },
			{ 0x0853, 0x00 },
			{ 0x0854, 0x00 },
			{ 0x0855, 0x00 },
			{ 0x0856, 0x00 },
			{ 0x0857, 0x00 },
			{ 0x0858, 0x00 },
			{ 0x0859, 0x00 },
			{ 0x085A, 0x00 },
			{ 0x085B, 0x00 },
			{ 0x085C, 0x00 },
			{ 0x085D, 0x00 },
			{ 0x085E, 0x00 },
			{ 0x085F, 0x00 },
			{ 0x0860, 0x00 },
			{ 0x0861, 0x00 },
			{ 0x090E, 0x02 },
			{ 0x0943, 0x01 },
			{ 0x0949, 0x0F },
			{ 0x094A, 0x0F },
			{ 0x094E, 0x49 },
			{ 0x094F, 0x02 },
			{ 0x095E, 0x00 },
			{ 0x0A02, 0x00 },
			{ 0x0A03, 0x1F },
			{ 0x0A04, 0x01 },
			{ 0x0A05, 0x1F },
			{ 0x0A14, 0x00 },
			{ 0x0A1A, 0x00 },
			{ 0x0A20, 0x00 },
			{ 0x0A26, 0x00 },
			{ 0x0A2C, 0x00 },
			{ 0x0B44, 0x2F },
			{ 0x0B46, 0x00 },
			{ 0x0B47, 0x00 },
			{ 0x0B48, 0x00 },
			{ 0x0B4A, 0x00 },
			{ 0x0B57, 0x0E },
			{ 0x0B58, 0x01 },
			/* End configuration registers */
			
			/* Start configuration postamble */
			{ 0x0514, 0x01 },
			{ 0x001C, 0x01 },
			{ 0x0540, 0x00 },
			{ 0x0B24, 0xC3 },
			{ 0x0B25, 0x02 },
			/* End configuration postamble */
		};
	
	write_register_by_size(regs[0].address,1,regs[0].value);
	write_register_by_size(regs[1].address,1,regs[1].value);
	write_register_by_size(regs[2].address,1,regs[2].value);
	usleep(400 * 1000);
	for(int i = 3; i< 526; i++)
	{
		write_register_by_size(regs[i].address,1,regs[i].value);
	}
	usleep(400 * 1000);
}//endfunction: set_registers_input_controlled    //in 0,1,2,3  -  1M





////used for debug purposes
///*! function used to populate the class with data from the si5345
//reads all dividers: P, Pxaxb, M, Mxaxb, N, R
//read frequencies: xtal,. dspll out, all outputs, intput;
//arguments: none
//returns: none. read values are stored in class member variables
//*/
//void si5345::all_dividers::read_all_dividers()
//{
//
//	freq_xtal = parent.get_xtal_freq();
//	freq_dspll_out = parent.get_dspll_out_freq();
//	freq_in = parent.get_in_freq();
//
//	/*  //unused for now
//	for(int i = 0 ; i < NR_INPUT_CHANNELS; i++)
//	{
//	    divider_P[i] = parent.read_divider_P(i);
//	    divider_P_numerator[i] = parent.read_reg_divider_P_numerator(i);
//	    divider_P_denominator[i] =  parent.read_reg_divider_P_denominator(i);
//	}
//
//	divider_M = parent.read_M();
//
//	divider_M_numerator = parent.read_reg_M_numerator();
//	divider_M_denominator = parent.read_reg_M_denominator();
//	//*/
//	divider_Pxaxb = parent.read_divider_pxaxb();
//
//	divider_Mxaxb = parent.read_MXAXB();
//	divider_Mxaxb_numerator = parent.read_reg_MXAXB_numerator();
//	divider_Mxaxb_denominator = parent.read_reg_MXAXB_denominator();
//
//	for (int i = 0; i < NR_MULTISYNTHS; i++) {
//		divider_N[i] = parent.read_divider_N(i);
//		divider_N_numerator[i] = parent.read_reg_divider_N_numerator(i);
//		divider_N_denominator[i] = parent.read_reg_divider_N_denominator(i);
//	}
//
//	for (int i = 0; i < NR_OUTPUT_CHANNELS; i++) {
//		divider_R[i] = parent.read_divider_R(i);
//		divider_R_denominator[i] = parent.read_divider_RX_denominator(i);
//	}
//
//	for (int i = 0; i < NR_OUTPUT_CHANNELS; i++) {
//		freq_out[i] = parent.read_freq_on_out_ch(i);
//	}
//
//	for (int i = 0; i < NR_OUTPUT_CHANNELS; i++) {
//		output_enabled[i] = parent.read_reg_output_en(i);
//	}
//
//}//endfunction: read_all_dividers
//
//unused currently
//
//
//
// other functions
// void si5345::set_bw_pll(uint8_t bw_pll_regs[6])
// {
//     int i;
//     for (i = 0; i < BW_PLL_REGS; i++)
//     {
//         mInterface->write_register(REG_BW_PLL_BASE + i, bw_pll_regs[i]);
//     }
//     mInterface->write_register(REG_BW_UPDATE_PLL, 1);
// }//endfunction: set_bw_pll
//
//
//
// void si5345::set_fast_bw_pll(uint8_t fast_bw_pll_regs[6])
// {
//     int i;
//     for (i = 0; i < FAST_BW_PLL_REGS; i++)
//     {
//         mInterface->write_register(REG_FAST_BW_PLL_BASE + i, fast_bw_pll_regs[i]);
//     }
//     mInterface->write_register(REG_BW_UPDATE_PLL, 1);
// }//endfunction: set_fast_bw_pll
//
//
//
// void si5345::set_holdexit_bw(uint8_t fast_bw_pll_regs[6])
// {
//     int i;
//     for (i = 0; i < HOLDEXIT_BW_PLL_REGS; i++)
//     {
//         mInterface->write_register(REG_HOLDEXIT_BW_PLL_BASE + i, fast_bw_pll_regs[i]);
//     }
// }//endfunction: set_holdexit_bw
//
//
//
// void si5345::set_hold_15m_cyc_count_pllb(uint32_t value)
// {
//     assert(value < HOLD_15M_CYC_COUNT_PLLB_MAX);
//     write_register_by_size(REG_HOLD_15M_CYC_COUNT_PLLB, HOLD_15M_CYC_COUNT_PLLB_SIZE, value);
// }//endfunction: set_hold_15m_cyc_count_pllb
//
//
//
// /*!   function used to check operation mode
// returns value of the HOLD status register
//     0= (input) locked mode; 1=freerun/holdover mode
// documentation: reference manual page 84
// */
// uint8_t si5345::check_holdover_freerun()
// {
//     uint8_t data = read_register(REG_HOLD);
//     return (data & REG_FIELD_HOLD_MASK) >> REG_FIELD_HOLD_POSITION;
// }//endfunction: check_holdover_freerun
//
//
//
// /*!   function used to set the force hold register
// arguments: command: 0=normal operation, 1=force holdover;
// documentation: reference manual page 114
// */
// void si5345::force_holdover(uint8_t command)
// {
//     mInterface->write_register(REG_FORCE_HOLD, command);
// }//endfunction: force_holdover
//
//
//
// /*!   function used to read the force hold register
// returns read value: 0=normal operation, 1=force holdover;
// documentation: reference manual page 114
// */
// uint8_t si5345::read_force_holdover()
// {
//     uint8_t data = read_register(REG_FORCE_HOLD);
//     return (data & REG_FORCE_HOLD_MASK);
// }//endfunction: read_force_holdover
//
//
//     //will be used later on

//
//
//
// /*!
// documentation: reference manual page 43
// */
// void si5345::set_vdd(uint8_t output_channel, uint8_t vdd_value)
// {
//     uint16_t reg_addr;
//     int data;
//
//     assert(output_channel < NR_OUTPUT_CHANNELS);
//
//     if (output_channel == 9) //special case
//     {
//         reg_addr = 0x013D;
//     }
//     else
//     {
//         reg_addr = REG_R_DIVX_MUX_SEL_BASE + REG_R_DIVX_MUX_SEL_OFFSET * output_channel;
//     }
//
//     data = read_register(reg_addr);
//     data = (data & ~OUTx_VDD_SEL_MASK);
//     data |= vdd_value;
//     mInterface->write_register(reg_addr, data);
// }//endfunction: set_vdd

#endif //si5345___cpp
