//
// Created by rpirea on 3/5/20.
//

#define REG_PAGE 0x00

#include "si5345_interface_i2c.hpp"

#include <cassert>
#include <cerrno>
#include <cstring>
#include <stdexcept>

#include <fcntl.h>
#include <i2c/smbus.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

si5345_interface_i2c::si5345_interface_i2c(const std::string &i2c_device, uint8_t i2c_address) {
    char exception_text[1024];

    i2c_dev = open(i2c_device.c_str(), O_RDWR);
    if (i2c_dev < 0) {
        sprintf(exception_text, "%s: %s\n", i2c_device.c_str(), strerror(errno));
        throw std::runtime_error(exception_text);
    }

    if (ioctl(i2c_dev, I2C_SLAVE, i2c_address) < 0) {
        sprintf(exception_text, "%hhu: %s\n", i2c_address, strerror(errno));
        throw std::runtime_error(exception_text);
    }
}

uint8_t si5345_interface_i2c::read_register_raw(uint8_t reg_addr) {
    int ret;

    ret = i2c_smbus_write_byte(i2c_dev, reg_addr);
    assert(ret >= 0);
    ret = i2c_smbus_read_byte(i2c_dev);
    assert(ret >= 0);

    return ret & 0xFF;
}

uint8_t si5345_interface_i2c::read_register(uint16_t reg_addr) {

    int ret;
    uint8_t page = reg_addr >> 8;
    uint8_t offset = reg_addr;

    ret = i2c_smbus_write_byte_data(i2c_dev, REG_PAGE, page);
    assert(ret >= 0);
    ret = i2c_smbus_write_byte(i2c_dev, offset);
    assert(ret >= 0);
    ret = i2c_smbus_read_byte(i2c_dev);
    assert(ret >= 0);

    return ret & 0xFF;
}

void si5345_interface_i2c::set_page(uint8_t page) {
    int ret;
    ret = i2c_smbus_write_byte_data(i2c_dev, REG_PAGE, page);
    assert(ret >= 0);
}

void si5345_interface_i2c::write_register(uint16_t reg_addr, uint8_t value) {
    int ret;
    uint8_t page = reg_addr >> 8;
    uint8_t offset = reg_addr;

    ret = i2c_smbus_write_byte_data(i2c_dev, REG_PAGE, page);
    assert(ret >= 0);
    ret = i2c_smbus_write_byte_data(i2c_dev, offset, value);
    assert(ret >= 0);
}
