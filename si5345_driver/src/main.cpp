#include <si5345/si5345.hpp>
#include <si5345/si5345_interface_i2c.hpp>
#include <si5345/si5345_interface_spi.hpp>
#include <cstdio>
#include <fcntl.h>
#include <cerrno>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <cstring>
#include <unistd.h>

void print_help() {
    printf("Usage:\n");
#ifndef SI5345_SPI
    printf("\t-a i2c slave address. Must be between [0-127]\n");
    printf("\t-d i2c device\n");
    printf("EXAMPLE: ./main -d /dev/i2c-0 -a 0x68\n");
#else
    printf("\t-d spi device\n");
    printf("EXAMPLE: ./main -d /dev/spidev0.1\n");
#endif
}

int main(int argc, char **argv) {

    if(argc < 3) {
        print_help();
        return 0;
    }

    int opt;
    char *filepath = NULL;
    si5345_interface *si5345Interface = nullptr;

#ifndef SI5345_SPI
    int file;
    unsigned slave_addr = -1;
    while ((opt = getopt(argc, argv, ":d:a:h")) != -1) {
        switch (opt) {
            case 'a':
                sscanf(optarg, "%u", &slave_addr); //we must add hex support
                if (slave_addr > 127) {
                    perror("ERROR: slave address must be between [0-127]\n");
                    return -EINVAL;
                }
                break;
            case 'd':
                filepath = new char[strlen(optarg)];
                strcpy(filepath, optarg);
                break;
            case 'h':
                print_help();
                return 0;
            case '?':
                fprintf(stderr, "unknown option: %c\n", optopt);
                print_help();
                return 0;
        }
    }

    if (filepath == NULL) {
        fprintf(stderr, "ERROR: missing i2c device(-d)\n");
        return -EINVAL;
    }

    file = open(filepath, O_RDWR);
    if (file < 0) {
        fprintf(stderr, "%s: %s\n", filepath, strerror(errno));
        return errno;
    }

    if (ioctl(file, I2C_SLAVE, slave_addr) < 0) {
        fprintf(stderr, "%hhu: %s\n", slave_addr, strerror(errno));
        return errno;
    }

    try{
        si5345Interface = new si5345_interface_i2c(filepath, slave_addr);
    }catch(std::runtime_error & ex) {
        printf("Exception when instantiating si5345 interface: %s", ex.what());
        return 0;
    }


#else

    while ((opt = getopt(argc, argv, ":d:h")) != -1) {
        switch (opt) {
            case 'd':
                filepath = new char[strlen(optarg)];
                strcpy(filepath, optarg);
                break;
            case 'h':
                print_help();
                return 0;
            case ':':
                fprintf(stderr,
                        "ERROR: -%c option needs a value\n",
                        optopt);
                return -EINVAL;
            case '?':
                fprintf(stderr, "unknown option: %c\n", optopt);
                print_help();
                return 0;

        }
    }

    try{
        si5345Interface = new si5345_interface_spi(filepath);
    }catch(std::runtime_error & ex) {
        printf("Exception when instantiating si5345 interface: %s", ex.what());
        return 0;
    }

#endif

    si5345 si5345_instance(si5345Interface, 48000000, 13500000000);

    printf("chip id %s\n", si5345_instance.read_id().c_str());

    return 0;
}
