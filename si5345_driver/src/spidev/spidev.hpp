//
// Created by rpirea on 07.11.2019.
//

#ifndef BCM5396_SPIDEV_HPP
#define BCM5396_SPIDEV_HPP

#include <linux/spi/spidev.h>
#include <string>

union spidev_config {
    struct {
        uint8_t spi_cpha:1;
        uint8_t spi_cpol:1;
        uint8_t spi_cs_high:1;
        uint8_t spi_lsb_first:1;
        uint8_t spi_3wire:1;
        uint8_t spi_loop:1;
        uint8_t spi_no_cs:1;
        uint8_t spi_ready:1;
    } bit_field;
    uint8_t spi_mode:2;
    uint8_t config;
};

class spidev {
    int mFd;
    std::string mDevice;
    spidev_config mConfig;
    uint32_t mSpeed;
    uint16_t mDelay;
    uint8_t mBitsPerWord;
public:

    spidev(std::string spidev_file, const spidev_config & config, uint32_t speed, uint16_t delay = 0,
           uint8_t bits_per_word = 8);

    void setDelay(uint16_t delay);

    uint16_t getDelay();

    void setSpeed(uint32_t speed);

    uint32_t getSpeed();

    void setBitsPerWord(uint8_t bits_per_word);

    uint8_t getBitsPerWord();

    void setConfig(const spidev_config & config);

    spidev_config getConfig();

    void write(uint8_t *tx_buf, uint32_t len);

    void read(uint8_t *rx_buf, uint32_t len);

    void transfer(uint8_t *tx_buf, uint8_t *rx_buf, uint32_t tx_len, uint32_t rx_len);

    void transferWriteRead(uint8_t *buf, uint32_t len);

};

#endif //BCM5396_SPIDEV_HPP
