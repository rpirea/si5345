//
// Created by rpirea on 07.11.2019.
//

#include "spidev.hpp"

#include <cassert>
#include <fcntl.h>
#include <sys/ioctl.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

spidev::spidev(std::string spidev_file, const spidev_config & config, uint32_t speed, uint16_t delay, uint8_t bits_per_word){
	mDevice = spidev_file;
	mFd = open(mDevice.c_str(), O_RDWR);
    assert(mFd >= 0);
    setDelay(delay);
    setSpeed(speed);
    setBitsPerWord(bits_per_word);
    setConfig(config);
}

void spidev::setDelay(uint16_t delay) {
    mDelay = delay;
}

uint16_t spidev::getDelay() {
    return mDelay;
}

void spidev::setSpeed(uint32_t speed) {
    int ret;

    ret = ioctl(mFd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    assert(ret >= 0);
    mSpeed = speed;
}

uint32_t spidev::getSpeed() {
    uint32_t speed;
    int ret;

    ret = ioctl(mFd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    assert(ret >= 0);

    return speed;
}

void spidev::setBitsPerWord(uint8_t bits_per_word) {
    int ret;

    ret = ioctl(mFd, SPI_IOC_WR_BITS_PER_WORD, &bits_per_word);
    assert(ret >= 0);
    mBitsPerWord = bits_per_word;
}

uint8_t spidev::getBitsPerWord() {
    uint8_t bits_per_word;
    int ret;

    ret = ioctl(mFd, SPI_IOC_RD_BITS_PER_WORD, &bits_per_word);
    assert(ret >= 0);

    return bits_per_word;
}

void spidev::setConfig(const spidev_config & config) {
    int ret;

    ret = ioctl(mFd, SPI_IOC_WR_MODE, &config.config);
    assert(ret >= 0);
    mConfig = config;
}

spidev_config spidev::getConfig() {
    spidev_config config;
    int ret;

    ret = ioctl(mFd, SPI_IOC_RD_MODE, &config.config);
    assert(ret >= 0);

    return config;
}

void spidev::write(uint8_t *tx_buf, uint32_t tx_len) {
    int ret;
    struct spi_ioc_transfer tr_wr;
    tr_wr.tx_buf = (unsigned long) tx_buf;
    tr_wr.rx_buf = (unsigned long) NULL;
    tr_wr.len = tx_len;
    tr_wr.delay_usecs = mDelay;
    tr_wr.speed_hz = mSpeed;
    tr_wr.bits_per_word = mBitsPerWord;
    tr_wr.cs_change = 0;
    tr_wr.tx_nbits = 0;
    tr_wr.rx_nbits = 0;
    tr_wr.pad = 0;

    ret = ioctl(mFd, SPI_IOC_MESSAGE(1), &tr_wr);
    assert (ret >= 1);
}

void spidev::read(uint8_t *rx_buf, uint32_t rx_len) {
    int ret;
    struct spi_ioc_transfer tr_rd;
    tr_rd.tx_buf = (unsigned long) NULL;
    tr_rd.rx_buf = (unsigned long) rx_buf;
    tr_rd.len = rx_len;
    tr_rd.delay_usecs = mDelay;
    tr_rd.speed_hz = mSpeed;
    tr_rd.bits_per_word = mBitsPerWord;
    tr_rd.cs_change = 0;
    tr_rd.tx_nbits = 0;
    tr_rd.rx_nbits = 0;
    tr_rd.pad = 0;

    ret = ioctl(mFd, SPI_IOC_MESSAGE(1), &tr_rd);
    assert (ret >= 1);
}

void spidev::transfer(uint8_t *tx_buf, uint8_t *rx_buf, uint32_t tx_len, uint32_t rx_len) {
    int ret;

    struct spi_ioc_transfer tr_wr;
    tr_wr.tx_buf = (unsigned long) tx_buf;
    tr_wr.rx_buf = (unsigned long) NULL;
    tr_wr.len = tx_len;
    tr_wr.delay_usecs = mDelay;
    tr_wr.speed_hz = mSpeed;
    tr_wr.bits_per_word = mBitsPerWord;
    tr_wr.cs_change = 0;
    tr_wr.tx_nbits = 0;
    tr_wr.rx_nbits = 0;
    tr_wr.pad = 0;

    struct spi_ioc_transfer tr_rd;
    tr_rd.tx_buf = (unsigned long) NULL;
    tr_rd.rx_buf = (unsigned long) rx_buf;
    tr_rd.len = rx_len;
    tr_rd.delay_usecs = mDelay;
    tr_rd.speed_hz = mSpeed;
    tr_rd.bits_per_word = mBitsPerWord;
    tr_rd.cs_change = 0;
    tr_rd.tx_nbits = 0;
    tr_rd.rx_nbits = 0;
    tr_rd.pad = 0;

    struct spi_ioc_transfer tr_buffs[2];
    tr_buffs[0] = tr_wr;
    tr_buffs[1] = tr_rd;
    ret = ioctl(mFd, SPI_IOC_MESSAGE(2), &tr_buffs);
    assert (ret >= 1);
}

void spidev::transferWriteRead(uint8_t *buf, uint32_t len) {
    int ret;
    struct spi_ioc_transfer tr;

    tr.tx_buf = (unsigned long) buf;
    tr.rx_buf = (unsigned long) buf;
    tr.len = len;
    tr.delay_usecs = mDelay;
    tr.speed_hz = mSpeed;
    tr.bits_per_word = mBitsPerWord;
    tr.cs_change = 0;
    tr.tx_nbits = 0;
    tr.rx_nbits = 0;
    tr.pad = 0;

    ret = ioctl(mFd, SPI_IOC_MESSAGE(1), &tr);
    assert (ret >= 1);
}

